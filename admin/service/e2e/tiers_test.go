package e2e_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	ierrors "github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/tier"
	"golang.org/x/xerrors"
)

func TestTiers(t *testing.T) {
	s := NewService(t)
	defer s.Finish()
	ti := tier.Tier{
		Name:                     "Name",
		MinChannelFollowersCount: 10,
		MaxChannelFollowersCount: 20,
		PostLikesCount:           10,
		PostSharesCount:          10,
		PostCommentsCount:        10,
	}
	t.Run("CreateTier", func(t *testing.T) {
		tiID, err := s.S.CreateTier(context.Background(), ti)
		require.NoError(t, err)
		assert.NotEmpty(t, tiID)
		ti.ID = tiID
	})
	t.Run("GetTier", func(t *testing.T) {
		dbc, err := s.S.GetTier(context.Background(), ti.ID)
		require.NoError(t, err)
		assert.Equal(t, ti, *dbc)
	})
	t.Run("GetTiers", func(t *testing.T) {
		tis, err := s.S.GetTiers(context.Background())
		require.NoError(t, err)
		assert.Equal(t, []*tier.Tier{&ti}, tis)
	})
	t.Run("UpdateTier", func(t *testing.T) {
		nti := tier.Tier{
			Name:                     "NewName",
			MinChannelFollowersCount: 10,
			MaxChannelFollowersCount: 20,
			PostLikesCount:           10,
			PostSharesCount:          10,
			PostCommentsCount:        10,
		}
		err := s.S.UpdateTier(context.Background(), ti.ID, nti)
		require.NoError(t, err)

		dbti, err := s.S.GetTier(context.Background(), ti.ID)
		require.NoError(t, err)
		nti.ID = ti.ID
		if assert.Equal(t, nti, *dbti) {
			ti = *dbti
		}
	})
	t.Run("DeleteTier", func(t *testing.T) {
		err := s.S.DeleteTier(context.Background(), ti.ID)
		require.NoError(t, err)

		dbc, err := s.S.GetTier(context.Background(), ti.ID)
		assert.Nil(t, dbc)
		assert.True(t, xerrors.Is(err, ierrors.NotFound), err)
	})
}
