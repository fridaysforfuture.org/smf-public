package e2e_test

import (
	"context"
	"database/sql"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/admin/service"
	"github.com/xescugc/smf/interops/mysql"
	wmock "github.com/xescugc/smf/worker/mock"
)

type Service struct {
	Worker *wmock.Service

	S  service.Service
	db *sql.DB

	Ctrl *gomock.Controller
}

func NewService(t *testing.T) Service {
	ctrl := gomock.NewController(t)

	db, err := mysql.New("mariadb", 3306, "root", "root123", mysql.Options{DBName: "smf_admin_test", ClientFoundRows: true, ParseTime: true})
	require.NoError(t, err)

	cr := mysql.NewChannelRepository(db)
	pr := mysql.NewPostRepository(db)
	tr := mysql.NewTierRepository(db)
	wr := mysql.NewWeightRepository(db)
	w := wmock.NewService(ctrl)
	ctx := context.Background()
	logger := log.NewNopLogger()

	a := service.New(ctx, cr, pr, tr, wr, w, logger)

	return Service{
		Worker: w,

		S:  a,
		db: db,

		Ctrl: ctrl,
	}
}

func (s *Service) Finish() {
	s.db.Close()
	s.Ctrl.Finish()
}
