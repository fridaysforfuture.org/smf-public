package e2e_test

import (
	"fmt"
	"log"
	"os"
	"testing"

	"github.com/xescugc/smf/interops/mysql"
	"github.com/xescugc/smf/interops/mysql/migrate"
)

func TestMain(m *testing.M) {
	db, err := mysql.New("mariadb", 3306, "root", "root123", mysql.Options{})
	if err != nil {
		log.Fatalf("%+v", err)
	}

	_, err = db.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", "smf_admin_test"))
	if err != nil {
		log.Fatalf("%+v", err)
	}

	db.Close()

	db, err = mysql.New("mariadb", 3306, "root", "root123", mysql.Options{DBName: "smf_admin_test", MultiStatements: true, ClientFoundRows: true, ParseTime: true})
	if err != nil {
		log.Fatalf("%+v", err)
	}

	err = migrate.Migrate(db)
	if err != nil {
		log.Fatalf("%+v", err)
	}

	db.Close()

	ret := m.Run()

	os.Exit(ret)
}
