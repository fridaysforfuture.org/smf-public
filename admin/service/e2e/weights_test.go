package e2e_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/weight"
	"golang.org/x/xerrors"
)

func TestWeights(t *testing.T) {
	s := NewService(t)
	defer s.Finish()
	w := weight.Weight{
		Type:       channel.Twitter,
		Percentage: 20,
	}
	t.Run("CreateWeight", func(t *testing.T) {
		wID, err := s.S.CreateWeight(context.Background(), w)
		require.NoError(t, err)
		assert.NotEmpty(t, wID)
		w.ID = wID
	})
	t.Run("GetWeight", func(t *testing.T) {
		dbc, err := s.S.GetWeight(context.Background(), w.ID)
		require.NoError(t, err)
		assert.Equal(t, w, *dbc)
	})
	t.Run("GetWeights", func(t *testing.T) {
		ws, err := s.S.GetWeights(context.Background())
		require.NoError(t, err)
		assert.Equal(t, []*weight.Weight{&w}, ws)
	})
	t.Run("UpdateWeight", func(t *testing.T) {
		nw := weight.Weight{
			Type:       channel.Twitter,
			Percentage: 30,
		}
		err := s.S.UpdateWeight(context.Background(), w.ID, nw)
		require.NoError(t, err)

		dbw, err := s.S.GetWeight(context.Background(), w.ID)
		require.NoError(t, err)
		nw.ID = w.ID
		if assert.Equal(t, nw, *dbw) {
			w = *dbw
		}
	})
	t.Run("DeleteWeight", func(t *testing.T) {
		err := s.S.DeleteWeight(context.Background(), w.ID)
		require.NoError(t, err)

		dbc, err := s.S.GetWeight(context.Background(), w.ID)
		assert.Nil(t, dbc)
		assert.True(t, xerrors.Is(err, ierrors.NotFound), err)
	})
}
