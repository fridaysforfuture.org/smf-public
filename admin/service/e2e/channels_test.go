package e2e_test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
	"golang.org/x/xerrors"
)

func TestChannels(t *testing.T) {
	s := NewService(t)
	defer s.Finish()
	c, err := channel.New("Spain", "http://twitter.com/xescugc")
	require.NoError(t, err)
	t.Run("CreateChannel", func(t *testing.T) {
		s.Worker.EXPECT().FetchChannel(gomock.Any(), gomock.Any()).Return(nil)
		s.Worker.EXPECT().FetchPosts(gomock.Any(), gomock.Any()).Return(nil)
		cID, err := s.S.CreateChannel(context.Background(), *c)
		require.NoError(t, err)
		assert.NotEmpty(t, cID)
		c.ID = cID
	})
	t.Run("GetChannel", func(t *testing.T) {
		dbc, err := s.S.GetChannel(context.Background(), c.ID)
		require.NoError(t, err)
		assert.Equal(t, c, dbc)
	})
	t.Run("GetChannels", func(t *testing.T) {
		cs, err := s.S.GetChannels(context.Background())
		require.NoError(t, err)
		assert.Equal(t, []*channel.Channel{c}, cs)
	})
	t.Run("UpdateChannel", func(t *testing.T) {
		newCountry := "France"
		err := s.S.UpdateChannel(context.Background(), c.ID, newCountry)
		require.NoError(t, err)

		dbc, err := s.S.GetChannel(context.Background(), c.ID)
		require.NoError(t, err)
		if assert.Equal(t, newCountry, dbc.Country) {
			c = dbc
		}
	})
	t.Run("DeleteChannel", func(t *testing.T) {
		err := s.S.DeleteChannel(context.Background(), c.ID)
		require.NoError(t, err)

		dbc, err := s.S.GetChannel(context.Background(), c.ID)
		assert.Nil(t, dbc)
		assert.True(t, xerrors.Is(err, ierrors.NotFound), err)
	})
}
