package service_test

import (
	"context"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/xescugc/smf/admin/service"
	imock "github.com/xescugc/smf/interops/mock"
	wmock "github.com/xescugc/smf/worker/mock"
)

func TestNew(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		cr := imock.NewChannelRepository(ctrl)
		pr := imock.NewPostRepository(ctrl)
		tr := imock.NewTierRepository(ctrl)
		wr := imock.NewWeightRepository(ctrl)
		w := wmock.NewService(ctrl)
		ctx := context.Background()
		logger := log.NewNopLogger()

		a := service.New(ctx, cr, pr, tr, wr, w, logger)
		assert.NotNil(t, a)
		assert.Implements(t, (*service.Service)(nil), a)
	})
}
