package service

import (
	"context"
	"fmt"
	"io"
	"time"

	"github.com/go-kit/kit/log"

	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/interops/tier"
	"github.com/xescugc/smf/interops/weight"
	worker "github.com/xescugc/smf/worker/service"
)

//go:generate mockgen -destination=../mock/service.go -mock_names=Service=Service -package mock github.com/xescugc/smf/admin/service Service

// Service is the main interface of the Admin service
// with all the functions provided
type Service interface {
	CreateChannel(ctx context.Context, c channel.Channel) (int, error)
	ImportChannels(ctx context.Context, ior io.ReadCloser) error
	GetChannels(ctx context.Context) ([]*channel.Channel, error)
	GetChannel(ctx context.Context, ID int) (*channel.Channel, error)
	UpdateChannel(ctx context.Context, ID int, country string) error
	DeleteChannel(ctx context.Context, ID int) error

	GetPosts(ctx context.Context) ([]*post.Post, error)
	GetPostsBySystems(ctx context.Context) ([]*post.WithChannel, error)
	GetPost(ctx context.Context, ID int) (*post.Post, error)
	DisablePost(ctx context.Context, ID int) error

	CreateTier(ctx context.Context, c tier.Tier) (int, error)
	GetTiers(ctx context.Context) ([]*tier.Tier, error)
	GetTier(ctx context.Context, ID int) (*tier.Tier, error)
	UpdateTier(ctx context.Context, ID int, t tier.Tier) error
	DeleteTier(ctx context.Context, ID int) error

	CreateWeight(ctx context.Context, c weight.Weight) (int, error)
	GetWeights(ctx context.Context) ([]*weight.Weight, error)
	GetWeight(ctx context.Context, ID int) (*weight.Weight, error)
	UpdateWeight(ctx context.Context, ID int, t weight.Weight) error
	DeleteWeight(ctx context.Context, ID int) error
}

// Admin is a struct that implements the Service
type Admin struct {
	channels channel.Repository
	posts    post.Repository
	tiers    tier.Repository
	weights  weight.Repository
	worker   worker.Service
	logger   log.Logger
}

// New returns a new Admin which implements the Service interface
func New(ctx context.Context, cr channel.Repository, pr post.Repository, tr tier.Repository, wr weight.Repository, w worker.Service, logger log.Logger) *Admin {
	a := &Admin{
		channels: cr,
		posts:    pr,
		tiers:    tr,
		weights:  wr,
		worker:   w,
		logger:   logger,
	}
	go a.deleteOldPosts(ctx)
	go a.refreshChannels(ctx)
	return a
}

// deleteOldPosts deletes all the posts older tha
// 2 months as we do not need to keep them
func (a *Admin) deleteOldPosts(ctx context.Context) {
	ticker := time.NewTicker(1 * time.Hour)
	for {
		select {
		case <-ticker.C:
			a.logger.Log("msg", "checking to delete old posts")
			err := a.posts.DeleteOlderThan(ctx, time.Now().Add(-(30*24*2)*time.Hour))
			if err != nil {
				a.logger.Log("error", err)
			}
		case <-ctx.Done():
			a.logger.Log("msg", "closing 'deleteOldPosts' goroutine")
			ticker.Stop()
			return
		}
	}
}

// refreshChannels refreshes the Channels information by
// refetching them from it's corresponding Source
func (a *Admin) refreshChannels(ctx context.Context) {
	ticker := time.NewTicker(1 * time.Hour)
	for {
		select {
		case <-ticker.C:
			a.logger.Log("msg", "checking to refresh Channels")
			chs, err := a.channels.FilterByLastRefresh(ctx, time.Now().Add(-24*time.Hour))
			if err != nil {
				a.logger.Log("error", err)
			}
			a.logger.Log("msg", fmt.Sprintf("refreshing %d Channels", len(chs)))
			for _, c := range chs {
				err = a.worker.FetchChannel(ctx, c.ID)
				if err != nil {
					a.logger.Log("error", err)
				}

				err = a.worker.FetchPosts(ctx, c.ID)
				if err != nil {
					a.logger.Log("error", err)
				}
			}
		case <-ctx.Done():
			a.logger.Log("msg", "closing 'refreshChannels' goroutine")
			ticker.Stop()
			return
		}
	}
}
