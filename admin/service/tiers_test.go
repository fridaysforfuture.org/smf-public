package service_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/tier"
)

func TestCreateTier(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			tID = 2
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)
		defer s.Finish()

		s.Tiers.EXPECT().Create(ctx, ti).Return(tID, nil)
		s.Tiers.EXPECT().Filter(ctx).Return(nil, nil)

		ID, err := s.S.CreateTier(ctx, ti)
		require.NoError(t, err)
		assert.Equal(t, tID, ID)
	})
	t.Run("OverlapingMin", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
			dti = tier.Tier{
				Name:                     "Ov",
				MinChannelFollowersCount: 11,
				MaxChannelFollowersCount: 30,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)
		defer s.Finish()

		s.Tiers.EXPECT().Filter(ctx).Return([]*tier.Tier{&dti}, nil)

		ID, err := s.S.CreateTier(ctx, ti)
		assert.Empty(t, ID)
		assert.EqualError(t, err, `it's overlapping with "Ov"`)
	})
	t.Run("OverlapingMax", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
			dti = tier.Tier{
				Name:                     "Ov",
				MinChannelFollowersCount: 5,
				MaxChannelFollowersCount: 15,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)
		defer s.Finish()

		s.Tiers.EXPECT().Filter(ctx).Return([]*tier.Tier{&dti}, nil)

		ID, err := s.S.CreateTier(ctx, ti)
		assert.Empty(t, ID)
		assert.EqualError(t, err, `it's overlapping with "Ov"`)
	})
	t.Run("OverlapingInside", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
			dti = tier.Tier{
				Name:                     "Ov",
				MinChannelFollowersCount: 12,
				MaxChannelFollowersCount: 15,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)
		defer s.Finish()

		s.Tiers.EXPECT().Filter(ctx).Return([]*tier.Tier{&dti}, nil)

		ID, err := s.S.CreateTier(ctx, ti)
		assert.Empty(t, ID)
		assert.EqualError(t, err, `it's overlapping with "Ov"`)
	})
}

func TestGetTiers(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)

		defer s.Finish()
		s.Tiers.EXPECT().Filter(ctx).Return([]*tier.Tier{&ti}, nil)

		tis, err := s.S.GetTiers(ctx)
		require.NoError(t, err)
		assert.Equal(t, []*tier.Tier{&ti}, tis)
	})
}

func TestGetTier(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			tID = 2
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)
		defer s.Finish()

		s.Tiers.EXPECT().Find(ctx, tID).Return(&ti, nil)

		tie, err := s.S.GetTier(ctx, tID)
		require.NoError(t, err)
		assert.Equal(t, &ti, tie)
	})
}

func TestUpdateTier(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			tID = 2
			ti  = tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			}
		)
		defer s.Finish()

		s.Tiers.EXPECT().Update(ctx, tID, ti).Return(nil)

		err := s.S.UpdateTier(ctx, tID, ti)
		require.NoError(t, err)
	})
}

func TestDeleteTier(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			tID = 2
		)
		defer s.Finish()

		s.Tiers.EXPECT().Delete(ctx, tID).Return(nil)

		err := s.S.DeleteTier(ctx, tID)
		require.NoError(t, err)
	})
}
