package service_test

import (
	"context"
	"io/ioutil"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
)

func TestCreateChannel(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			cID = 2
		)
		defer s.Finish()
		c, err := channel.New("Spain", "http://twitter.com/xescugc")
		require.NoError(t, err)

		s.Channels.EXPECT().Create(ctx, *c).Return(cID, nil)
		s.Worker.EXPECT().FetchChannel(ctx, cID).Return(nil)
		s.Worker.EXPECT().FetchPosts(ctx, cID).Return(nil)

		ID, err := s.S.CreateChannel(ctx, *c)
		require.NoError(t, err)
		assert.Equal(t, cID, ID)
	})
}

func TestImportChannels(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s    = NewService(t)
			ctx  = context.Background()
			cID2 = 2
			in   = `country,link
"Spain","http://twitter.com/xescugc"
"Spain","https://twitter.com/FridayForMadrid"
`
			iorc = ioutil.NopCloser(strings.NewReader(in))
		)
		defer s.Finish()

		c, err := channel.New("Spain", "http://twitter.com/xescugc")
		require.NoError(t, err)

		c2, err := channel.New("Spain", "https://twitter.com/FridayForMadrid")
		require.NoError(t, err)

		s.Channels.EXPECT().FindByLink(ctx, c.Link).Return(c, nil)

		s.Channels.EXPECT().FindByLink(ctx, c2.Link).Return(nil, ierrors.NotFound)
		s.Channels.EXPECT().Create(ctx, *c2).Return(cID2, nil)
		s.Worker.EXPECT().FetchChannel(ctx, cID2).Return(nil)
		s.Worker.EXPECT().FetchPosts(ctx, cID2).Return(nil)

		err = s.S.ImportChannels(ctx, iorc)
		require.NoError(t, err)
	})
}

func TestGetChannels(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
		)
		defer s.Finish()
		c, err := channel.New("Spain", "http://twitter.com/xescugc")
		require.NoError(t, err)

		s.Channels.EXPECT().Filter(ctx).Return([]*channel.Channel{c}, nil)

		chs, err := s.S.GetChannels(ctx)
		require.NoError(t, err)
		assert.Equal(t, []*channel.Channel{c}, chs)
	})
}

func TestGetChannel(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			cID = 2
		)
		defer s.Finish()

		c, err := channel.New("Spain", "http://twitter.com/xescugc")
		require.NoError(t, err)

		s.Channels.EXPECT().Find(ctx, cID).Return(c, nil)

		ch, err := s.S.GetChannel(ctx, cID)
		require.NoError(t, err)
		assert.Equal(t, c, ch)
	})
}

func TestUpdateChannel(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			cID = 2
		)
		defer s.Finish()

		c, err := channel.New("Spain", "http://twitter.com/xescugc")
		require.NoError(t, err)

		nc, err := channel.New("France", "http://twitter.com/xescugc")
		require.NoError(t, err)

		s.Channels.EXPECT().Find(ctx, cID).Return(c, nil)
		s.Channels.EXPECT().Update(ctx, cID, *nc).Return(nil)

		err = s.S.UpdateChannel(ctx, cID, "France")
		require.NoError(t, err)
	})
}

func TestDeleteChannel(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			cID = 2
		)
		defer s.Finish()

		s.Posts.EXPECT().DeleteByChannelID(ctx, cID).Return(nil)
		s.Channels.EXPECT().Delete(ctx, cID).Return(nil)

		err := s.S.DeleteChannel(ctx, cID)
		require.NoError(t, err)
	})
}
