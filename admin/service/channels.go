package service

import (
	"context"
	"encoding/csv"
	"errors"
	"io"
	"strings"

	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
	"golang.org/x/xerrors"
)

// CreateChannel creates a new Channel c, it returns the ID
func (a *Admin) CreateChannel(ctx context.Context, c channel.Channel) (int, error) {
	if err := c.Validate(); err != nil {
		return 0, xerrors.Errorf("Channel validation failed: %w", err)
	}

	id, err := a.channels.Create(ctx, c)
	if err != nil {
		return 0, xerrors.Errorf("chould not create Channel: %w", err)
	}

	err = a.worker.FetchChannel(ctx, id)
	if err != nil {
		return 0, xerrors.Errorf("chould not fetch Channel: %w", err)
	}

	err = a.worker.FetchPosts(ctx, id)
	if err != nil {
		return 0, xerrors.Errorf("chould not fetch Posts: %w", err)
	}

	return id, nil
}

// ImportChannels imports the Channels from the ior, the columns
// have to be 'Country','Link'
func (a *Admin) ImportChannels(ctx context.Context, ior io.ReadCloser) error {
	defer ior.Close()
	csvr := csv.NewReader(ior)
	csvr.TrimLeadingSpace = true
	csvr.LazyQuotes = true
	// Number of lines read
	var count int
	warnings := make([]string, 0)
	for {
		record, err := csvr.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		count++
		if count == 1 {
			// First line it's the title of the columns so we should skip it
			continue
		}

		c, err := channel.New(record[0], record[1])
		if err != nil {
			warnings = append(warnings, xerrors.Errorf("failed to initialize Channel on line %d: %w", count+1, err).Error())
			continue
		}

		// If the Channel exists we do not create it again
		// we'll ignore it and let the BG job update it
		// if needed
		_, err = a.channels.FindByLink(ctx, c.Link)
		if errors.Is(err, ierrors.NotFound) {
			_, err = a.CreateChannel(ctx, *c)
			if err != nil {
				warnings = append(warnings, xerrors.Errorf("failed to create Channel on line %d: %w", count+1, err).Error())
			}
			continue
		} else if err != nil {
			warnings = append(warnings, xerrors.Errorf("failed to initialize Channel on line %d: %w", count+1, err).Error())
			continue
		}
	}

	if len(warnings) != 0 {
		return xerrors.New(strings.Join(warnings, ", "))
	}
	return nil
}

// GetChannels returns all the Channels
func (a *Admin) GetChannels(ctx context.Context) ([]*channel.Channel, error) {
	chs, err := a.channels.Filter(ctx)
	if err != nil {
		return nil, xerrors.Errorf("failed to filter Channels: %w", err)
	}

	return chs, nil
}

// GetChannel returns the Channel with the ID
func (a *Admin) GetChannel(ctx context.Context, ID int) (*channel.Channel, error) {
	c, err := a.channels.Find(ctx, ID)
	if err != nil {
		return nil, xerrors.Errorf("failed to find Channel: %w", err)
	}

	return c, nil
}

// UpdateChannel updates the Channel with the ID with the new country.
// Only the country can be updated, if the link needs to be updated then
// delete and recreate
func (a *Admin) UpdateChannel(ctx context.Context, ID int, country string) error {
	dbc, err := a.channels.Find(ctx, ID)
	if err != nil {
		return xerrors.Errorf("failed to find Channel: %w", err)
	}

	dbc.Country = country

	if err = dbc.Validate(); err != nil {
		return xerrors.Errorf("Channel validation failed: %w", err)
	}

	err = a.channels.Update(ctx, ID, *dbc)
	if err != nil {
		return xerrors.Errorf("failed to Update Channel with ID %d: %w", ID, err)
	}

	return nil
}

// DeleteChannel deletes the Channel with ID
func (a *Admin) DeleteChannel(ctx context.Context, ID int) error {
	err := a.posts.DeleteByChannelID(ctx, ID)
	if err != nil && !xerrors.Is(err, ierrors.NotFound) {
		return xerrors.Errorf("failed to delete Channel: %w", err)
	}

	err = a.channels.Delete(ctx, ID)
	if err != nil {
		return xerrors.Errorf("failed to delete Channel: %w", err)
	}

	return nil
}
