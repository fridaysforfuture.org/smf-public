package service

import (
	"context"

	"github.com/xescugc/smf/interops/post"
	"golang.org/x/xerrors"
)

// GetPosts retuns all the Posts
func (a *Admin) GetPosts(ctx context.Context) ([]*post.Post, error) {
	ps, err := a.posts.Filter(ctx)
	if err != nil {
		return nil, xerrors.Errorf("failed to filter Posts: %w", err)
	}
	return ps, nil
}

// GetPostsBySystems retuns all the Posts with the filter by the
// defined Systems
func (a *Admin) GetPostsBySystems(ctx context.Context) ([]*post.WithChannel, error) {
	ws, err := a.weights.Filter(ctx)
	if err != nil {
		return nil, xerrors.Errorf("failed to filter Weights: %w", err)
	}

	ps, err := a.posts.FilterBySystems(ctx, ws)
	if err != nil {
		return nil, xerrors.Errorf("failed to filter by sytem Posts: %w", err)
	}
	return ps, nil
}

// GetPost returns the Post with ID
func (a *Admin) GetPost(ctx context.Context, ID int) (*post.Post, error) {
	p, err := a.posts.Find(ctx, ID)
	if err != nil {
		return nil, xerrors.Errorf("failed to find Post with id %d: %w", ID, err)
	}
	return p, nil
}

// DisablePost sets the current post as disabled
func (a *Admin) DisablePost(ctx context.Context, ID int) error {
	p, err := a.posts.Find(ctx, ID)
	if err != nil {
		return xerrors.Errorf("failed to find Post with id %d: %w", ID, err)
	}

	p.Enabled = false

	err = a.posts.Update(ctx, ID, *p)
	if err != nil {
		return xerrors.Errorf("failed to update post %d: %w", ID, err)
	}

	return nil
}
