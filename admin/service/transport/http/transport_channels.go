package http

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func decodeChannelsIndexRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return channelsIndexRequest{}, nil
}

func encodeChannelsIndexResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("channels/index.html")
	res := response.(channelsIndexResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Channels,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeChannelsNewRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return channelsNewRequest{}, nil
}

func encodeChannelsNewResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("channels/new.html")
	_ = response.(channelsNewResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: nil,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeChannelsImportRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return channelsImportRequest{}, nil
}

func encodeChannelsImportResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("channels/import.html")
	_ = response.(channelsImportResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: nil,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeChannelsImportCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	file, _, err := r.FormFile("file")
	if err != nil {
		return nil, err
	}
	defer file.Close()

	b, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return channelsImportCreateRequest{
		File: ioutil.NopCloser(bytes.NewBuffer(b)),
	}, nil
}

func encodeChannelsImportCreateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("channels/import.html")
	_ = response.(channelsImportCreateResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: nil,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeChannelsCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}
	return channelsCreateRequest{
		Country: r.PostForm.Get("country"),
		Link:    r.PostForm.Get("link"),
	}, nil
}

func encodeChannelsCreateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(channelsCreateResponse)

	w.Header().Set("Location", fmt.Sprintf("/channels"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}

func decodeChannelsShowRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return channelsShowRequest{
		ID: ID,
	}, nil
}

func encodeChannelsShowResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("channels/show.html")
	res := response.(channelsShowResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Channel,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeChannelsEditRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return channelsEditRequest{
		ID: ID,
	}, nil
}

func encodeChannelsEditResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("channels/edit.html")
	res := response.(channelsEditResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Channel,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeChannelsUpdateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}

	vars := mux.Vars(r)

	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}

	return channelsUpdateRequest{
		ID:      ID,
		Country: r.PostForm.Get("country"),
	}, nil
}

func encodeChannelsUpdateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(channelsUpdateResponse)

	w.Header().Set("Location", fmt.Sprintf("/channels"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}

func decodeChannelsDeleteRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return channelsDeleteRequest{
		ID: ID,
	}, nil
}

func encodeChannelsDeleteResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	//t := getTemplate("channels/index.html")
	//_ = response.(channelsDeleteResponse)

	//b := bytes.Buffer{}
	//err := t.Execute(&b, Data{
	//Data: nil,
	//})
	//if err != nil {
	//return err
	//}

	w.Header().Set("Location", fmt.Sprintf("/channels"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}
