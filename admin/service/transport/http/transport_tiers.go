package http

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func decodeTiersIndexRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return tiersIndexRequest{}, nil
}

func encodeTiersIndexResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("tiers/index.html")
	res := response.(tiersIndexResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Tiers,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeTiersNewRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return tiersNewRequest{}, nil
}

func encodeTiersNewResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("tiers/new.html")
	_ = response.(tiersNewResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: nil,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeTiersCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}
	mincfc, _ := strconv.Atoi(r.PostForm.Get("min_channel_followers_count"))
	maxcfc, _ := strconv.Atoi(r.PostForm.Get("max_channel_followers_count"))
	plc, _ := strconv.Atoi(r.PostForm.Get("post_likes_count"))
	psc, _ := strconv.Atoi(r.PostForm.Get("post_shares_count"))
	pcc, _ := strconv.Atoi(r.PostForm.Get("post_comments_count"))
	return tiersCreateRequest{
		Name: r.PostForm.Get("name"),

		MinChannelFollowersCount: mincfc,
		MaxChannelFollowersCount: maxcfc,

		PostLikesCount:    plc,
		PostSharesCount:   psc,
		PostCommentsCount: pcc,
	}, nil
}

func encodeTiersCreateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(tiersCreateResponse)

	w.Header().Set("Location", fmt.Sprintf("/tiers"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}

func decodeTiersEditRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return tiersEditRequest{
		ID: ID,
	}, nil
}

func encodeTiersEditResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("tiers/edit.html")
	res := response.(tiersEditResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Tier,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeTiersUpdateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}

	vars := mux.Vars(r)

	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}

	mincfc, _ := strconv.Atoi(r.PostForm.Get("min_channel_followers_count"))
	maxcfc, _ := strconv.Atoi(r.PostForm.Get("max_channel_followers_count"))
	plc, _ := strconv.Atoi(r.PostForm.Get("post_likes_count"))
	psc, _ := strconv.Atoi(r.PostForm.Get("post_shares_count"))
	pcc, _ := strconv.Atoi(r.PostForm.Get("post_comments_count"))
	return tiersUpdateRequest{
		ID:   ID,
		Name: r.PostForm.Get("name"),

		MinChannelFollowersCount: mincfc,
		MaxChannelFollowersCount: maxcfc,

		PostLikesCount:    plc,
		PostSharesCount:   psc,
		PostCommentsCount: pcc,
	}, nil

}

func encodeTiersUpdateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(tiersUpdateResponse)

	w.Header().Set("Location", fmt.Sprintf("/tiers"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}

func decodeTiersDeleteRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return tiersDeleteRequest{
		ID: ID,
	}, nil
}

func encodeTiersDeleteResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	//t := getTemplate("tiers/index.html")
	//_ = response.(tiersDeleteResponse)

	//b := bytes.Buffer{}
	//err := t.Execute(&b, Data{
	//Data: nil,
	//})
	//if err != nil {
	//return err
	//}

	w.Header().Set("Location", fmt.Sprintf("/tiers"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}
