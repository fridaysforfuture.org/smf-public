package http

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/xescugc/smf/admin/service"
	"github.com/xescugc/smf/interops/post"
)

type postsIndexRequest struct {
}

type postsIndexResponse struct {
	Posts []*post.Post
}

func makePostsIndexEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(postsIndexRequest)
		ps, err := s.GetPosts(ctx)
		if err != nil {
			return nil, err
		}

		return postsIndexResponse{
			Posts: ps,
		}, nil
	}
}

type postsIndexSystemRequest struct {
}

type postsIndexSystemResponse struct {
	Posts []*post.WithChannel
}

func makePostsIndexSystemEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(postsIndexSystemRequest)
		ps, err := s.GetPostsBySystems(ctx)
		if err != nil {
			return nil, err
		}

		return postsIndexSystemResponse{
			Posts: ps,
		}, nil
	}
}

type postsShowRequest struct {
	ID int
}

type postsShowResponse struct {
	Post *post.Post
}

func makePostsShowEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(postsShowRequest)
		p, err := s.GetPost(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return postsShowResponse{
			Post: p,
		}, nil
	}
}

type postsDisableRequest struct {
	ID int
}

type postsDisableResponse struct {
}

func makePostsDisableEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(postsDisableRequest)
		err := s.DisablePost(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return postsDisableResponse{}, nil
	}
}
