package http

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func decodePostsIndexRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return postsIndexRequest{}, nil
}

func encodePostsIndexResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("posts/index.html")
	res := response.(postsIndexResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Posts,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodePostsIndexSystemRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return postsIndexSystemRequest{}, nil
}

func encodePostsIndexSystemResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("posts/index_system.html")
	res := response.(postsIndexSystemResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Posts,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodePostsShowRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return postsShowRequest{
		ID: ID,
	}, nil
}

func encodePostsShowResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("posts/show.html")
	res := response.(postsShowResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Post,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodePostsDisableRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return postsDisableRequest{
		ID: ID,
	}, nil
}

func encodePostsDisableResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(postsDisableResponse)

	w.Header().Set("Location", fmt.Sprintf("/posts"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}
