package http

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/xescugc/smf/admin/service"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/weight"
)

type weightsIndexRequest struct {
}

type weightsIndexResponse struct {
	Weights []*weight.Weight
}

func makeWeightsIndexEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(weightsIndexRequest)
		ws, err := s.GetWeights(ctx)
		if err != nil {
			return nil, err
		}

		return weightsIndexResponse{
			Weights: ws,
		}, nil
	}
}

type weightsNewRequest struct {
}

type weightsNewResponse struct {
}

func makeWeightsNewEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(weightsNewRequest)
		return weightsNewResponse{}, nil
	}
}

type weightsCreateRequest struct {
	Type       string
	Percentage int
}

type weightsCreateResponse struct {
}

func makeWeightsCreateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(weightsCreateRequest)
		ty, err := channel.TypeString(req.Type)
		if err != nil {
			return nil, err
		}
		w := weight.Weight{
			Type:       ty,
			Percentage: req.Percentage,
		}
		_, err = s.CreateWeight(ctx, w)
		if err != nil {
			return nil, err
		}

		return weightsCreateResponse{}, nil
	}
}

type weightsEditRequest struct {
	ID int
}

type weightsEditResponse struct {
	Weight *weight.Weight
}

func makeWeightsEditEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(weightsEditRequest)
		w, err := s.GetWeight(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return weightsEditResponse{
			Weight: w,
		}, nil
	}
}

type weightsUpdateRequest struct {
	ID         int
	Type       string
	Percentage int
}

type weightsUpdateResponse struct {
}

func makeWeightsUpdateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(weightsUpdateRequest)
		ty, err := channel.TypeString(req.Type)
		if err != nil {
			return nil, err
		}
		w := weight.Weight{
			Type:       ty,
			Percentage: req.Percentage,
		}
		err = s.UpdateWeight(ctx, req.ID, w)
		if err != nil {
			return nil, err
		}

		return weightsUpdateResponse{}, nil
	}
}

type weightsDeleteRequest struct {
	ID int
}

type weightsDeleteResponse struct {
}

func makeWeightsDeleteEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(weightsDeleteRequest)
		err := s.DeleteWeight(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return weightsDeleteResponse{}, nil
	}
}
