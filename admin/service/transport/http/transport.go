package http

import (
	"bytes"
	"context"
	"io"
	"net/http"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
	"github.com/xescugc/smf/admin/service"
)

// MakeHandler retuns an http.Handler of the Service s functions
func MakeHandler(s service.Service) http.Handler {
	homeHandler := kithttp.NewServer(
		makeHomeEndpoint(s),
		decodeHomeRequest,
		encodeHomeResponse,
	)

	channelsIndexHandler := kithttp.NewServer(
		makeChannelsIndexEndpoint(s),
		decodeChannelsIndexRequest,
		encodeChannelsIndexResponse,
	)
	channelsNewHandler := kithttp.NewServer(
		makeChannelsNewEndpoint(s),
		decodeChannelsNewRequest,
		encodeChannelsNewResponse,
	)
	channelsImportHandler := kithttp.NewServer(
		makeChannelsImportEndpoint(s),
		decodeChannelsImportRequest,
		encodeChannelsImportResponse,
	)
	channelsImportCreateHandler := kithttp.NewServer(
		makeChannelsImportCreateEndpoint(s),
		decodeChannelsImportCreateRequest,
		encodeChannelsImportCreateResponse,
	)
	channelsCreateHandler := kithttp.NewServer(
		makeChannelsCreateEndpoint(s),
		decodeChannelsCreateRequest,
		encodeChannelsCreateResponse,
	)
	channelsShowHandler := kithttp.NewServer(
		makeChannelsShowEndpoint(s),
		decodeChannelsShowRequest,
		encodeChannelsShowResponse,
	)
	channelsEditHandler := kithttp.NewServer(
		makeChannelsEditEndpoint(s),
		decodeChannelsEditRequest,
		encodeChannelsEditResponse,
	)
	channelsUpdateHandler := kithttp.NewServer(
		makeChannelsUpdateEndpoint(s),
		decodeChannelsUpdateRequest,
		encodeChannelsUpdateResponse,
	)
	channelsDeleteHandler := kithttp.NewServer(
		makeChannelsDeleteEndpoint(s),
		decodeChannelsDeleteRequest,
		encodeChannelsDeleteResponse,
	)

	postsIndexHandler := kithttp.NewServer(
		makePostsIndexEndpoint(s),
		decodePostsIndexRequest,
		encodePostsIndexResponse,
	)
	postsIndexSystemHandler := kithttp.NewServer(
		makePostsIndexSystemEndpoint(s),
		decodePostsIndexSystemRequest,
		encodePostsIndexSystemResponse,
	)
	postsShowHandler := kithttp.NewServer(
		makePostsShowEndpoint(s),
		decodePostsShowRequest,
		encodePostsShowResponse,
	)
	postsDisableHandler := kithttp.NewServer(
		makePostsDisableEndpoint(s),
		decodePostsDisableRequest,
		encodePostsDisableResponse,
	)

	tiersIndexHandler := kithttp.NewServer(
		makeTiersIndexEndpoint(s),
		decodeTiersIndexRequest,
		encodeTiersIndexResponse,
	)
	tiersNewHandler := kithttp.NewServer(
		makeTiersNewEndpoint(s),
		decodeTiersNewRequest,
		encodeTiersNewResponse,
	)
	tiersCreateHandler := kithttp.NewServer(
		makeTiersCreateEndpoint(s),
		decodeTiersCreateRequest,
		encodeTiersCreateResponse,
	)
	tiersEditHandler := kithttp.NewServer(
		makeTiersEditEndpoint(s),
		decodeTiersEditRequest,
		encodeTiersEditResponse,
	)
	tiersUpdateHandler := kithttp.NewServer(
		makeTiersUpdateEndpoint(s),
		decodeTiersUpdateRequest,
		encodeTiersUpdateResponse,
	)
	tiersDeleteHandler := kithttp.NewServer(
		makeTiersDeleteEndpoint(s),
		decodeTiersDeleteRequest,
		encodeTiersDeleteResponse,
	)

	weightsIndexHandler := kithttp.NewServer(
		makeWeightsIndexEndpoint(s),
		decodeWeightsIndexRequest,
		encodeWeightsIndexResponse,
	)
	weightsNewHandler := kithttp.NewServer(
		makeWeightsNewEndpoint(s),
		decodeWeightsNewRequest,
		encodeWeightsNewResponse,
	)
	weightsCreateHandler := kithttp.NewServer(
		makeWeightsCreateEndpoint(s),
		decodeWeightsCreateRequest,
		encodeWeightsCreateResponse,
	)
	weightsEditHandler := kithttp.NewServer(
		makeWeightsEditEndpoint(s),
		decodeWeightsEditRequest,
		encodeWeightsEditResponse,
	)
	weightsUpdateHandler := kithttp.NewServer(
		makeWeightsUpdateEndpoint(s),
		decodeWeightsUpdateRequest,
		encodeWeightsUpdateResponse,
	)
	weightsDeleteHandler := kithttp.NewServer(
		makeWeightsDeleteEndpoint(s),
		decodeWeightsDeleteRequest,
		encodeWeightsDeleteResponse,
	)

	r := mux.NewRouter()

	r.Handle("/", homeHandler).Methods("GET")
	r.Handle("/channels", channelsIndexHandler).Methods("GET")
	r.Handle("/channels/new", channelsNewHandler).Methods("GET")
	r.Handle("/channels/import", channelsImportHandler).Methods("GET")
	r.Handle("/channels/import", channelsImportCreateHandler).Methods("POST")
	r.Handle("/channels", channelsCreateHandler).Methods("POST")
	r.Handle("/channels/{id}", channelsShowHandler).Methods("GET")
	r.Handle("/channels/{id}/edit", channelsEditHandler).Methods("GET")
	// Can not be PUT as it's not HTML complient
	r.Handle("/channels/{id}", channelsUpdateHandler).Methods("POST")
	// Can not be DELETE as it's not HTML complient alson it should
	// not have the '/delete' but it's the only way to make it not
	// repetable with the Update endpoint
	r.Handle("/channels/{id}/delete", channelsDeleteHandler).Methods("POST")

	r.Handle("/posts", postsIndexHandler).Methods("GET")
	r.Handle("/posts-system", postsIndexSystemHandler).Methods("GET")
	r.Handle("/posts/{id}", postsShowHandler).Methods("GET")
	r.Handle("/posts/{id}/disable", postsDisableHandler).Methods("POST")

	r.Handle("/tiers", tiersIndexHandler).Methods("GET")
	r.Handle("/tiers/new", tiersNewHandler).Methods("GET")
	r.Handle("/tiers", tiersCreateHandler).Methods("POST")
	r.Handle("/tiers/{id}/edit", tiersEditHandler).Methods("GET")
	r.Handle("/tiers/{id}", tiersUpdateHandler).Methods("POST")
	r.Handle("/tiers/{id}/delete", tiersDeleteHandler).Methods("POST")

	r.Handle("/weights", weightsIndexHandler).Methods("GET")
	r.Handle("/weights/new", weightsNewHandler).Methods("GET")
	r.Handle("/weights", weightsCreateHandler).Methods("POST")
	r.Handle("/weights/{id}/edit", weightsEditHandler).Methods("GET")
	r.Handle("/weights/{id}", weightsUpdateHandler).Methods("POST")
	r.Handle("/weights/{id}/delete", weightsDeleteHandler).Methods("POST")

	return r
}

func decodeHomeRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return homeRequest{}, nil
}

func encodeHomeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("home/index.html")
	res := response.(homeResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}
