package http

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/xescugc/smf/admin/service"
	"github.com/xescugc/smf/interops/tier"
)

type tiersIndexRequest struct {
}

type tiersIndexResponse struct {
	Tiers []*tier.Tier
}

func makeTiersIndexEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(tiersIndexRequest)
		ts, err := s.GetTiers(ctx)
		if err != nil {
			return nil, err
		}

		return tiersIndexResponse{
			Tiers: ts,
		}, nil
	}
}

type tiersNewRequest struct {
}

type tiersNewResponse struct {
}

func makeTiersNewEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(tiersNewRequest)
		return tiersNewResponse{}, nil
	}
}

type tiersCreateRequest struct {
	Name string

	MinChannelFollowersCount int
	MaxChannelFollowersCount int

	PostLikesCount    int
	PostSharesCount   int
	PostCommentsCount int
}

type tiersCreateResponse struct {
}

func makeTiersCreateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(tiersCreateRequest)
		t := tier.Tier{
			Name: req.Name,

			MinChannelFollowersCount: req.MinChannelFollowersCount,
			MaxChannelFollowersCount: req.MaxChannelFollowersCount,

			PostLikesCount:    req.PostLikesCount,
			PostSharesCount:   req.PostSharesCount,
			PostCommentsCount: req.PostCommentsCount,
		}
		_, err := s.CreateTier(ctx, t)
		if err != nil {
			return nil, err
		}

		return tiersCreateResponse{}, nil
	}
}

type tiersEditRequest struct {
	ID int
}

type tiersEditResponse struct {
	Tier *tier.Tier
}

func makeTiersEditEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(tiersEditRequest)
		t, err := s.GetTier(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return tiersEditResponse{
			Tier: t,
		}, nil
	}
}

type tiersUpdateRequest struct {
	ID   int
	Name string

	MinChannelFollowersCount int
	MaxChannelFollowersCount int

	PostLikesCount    int
	PostSharesCount   int
	PostCommentsCount int
}

type tiersUpdateResponse struct {
}

func makeTiersUpdateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(tiersUpdateRequest)
		t := tier.Tier{
			Name: req.Name,

			MinChannelFollowersCount: req.MinChannelFollowersCount,
			MaxChannelFollowersCount: req.MaxChannelFollowersCount,

			PostLikesCount:    req.PostLikesCount,
			PostSharesCount:   req.PostSharesCount,
			PostCommentsCount: req.PostCommentsCount,
		}
		err := s.UpdateTier(ctx, req.ID, t)
		if err != nil {
			return nil, err
		}

		return tiersUpdateResponse{}, nil
	}
}

type tiersDeleteRequest struct {
	ID int
}

type tiersDeleteResponse struct {
}

func makeTiersDeleteEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(tiersDeleteRequest)
		err := s.DeleteTier(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return tiersDeleteResponse{}, nil
	}
}
