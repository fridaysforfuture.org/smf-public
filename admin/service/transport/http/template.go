package http

import (
	template "github.com/arschles/go-bindata-html-template"
)

//go:generate go-bindata -nometadata -ignore=bindata\.go -ignore=doc\.go -ignore=assets\.go -pkg http ./template/...

func getTemplate(path string) *template.Template {
	if path == "" {
		panic("Not enought arguemnts to the getTemplate()")
	}

	funcs := template.FuncMap{}

	t, err := template.New("base.html", Asset).Funcs(funcs).ParseFiles(
		"template/index.html",
		"template/header.html",
		"template/sidebar.html",
		"template/"+path,
	)

	if err != nil {
		panic(err)
	}

	return t
}
