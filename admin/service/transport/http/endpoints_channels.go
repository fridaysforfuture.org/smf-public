package http

import (
	"context"
	"io"

	"github.com/go-kit/kit/endpoint"
	"github.com/xescugc/smf/admin/service"
	"github.com/xescugc/smf/interops/channel"
)

type channelsIndexRequest struct {
}

type channelsIndexResponse struct {
	Channels []*channel.Channel
}

func makeChannelsIndexEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(channelsIndexRequest)
		chs, err := s.GetChannels(ctx)
		if err != nil {
			return nil, err
		}

		return channelsIndexResponse{
			Channels: chs,
		}, nil
	}
}

type channelsNewRequest struct {
}

type channelsNewResponse struct {
}

func makeChannelsNewEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(channelsNewRequest)
		return channelsNewResponse{}, nil
	}
}

type channelsImportRequest struct {
}

type channelsImportResponse struct {
}

func makeChannelsImportEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(channelsImportRequest)
		return channelsImportResponse{}, nil
	}
}

type channelsImportCreateRequest struct {
	File io.ReadCloser
}

type channelsImportCreateResponse struct {
}

func makeChannelsImportCreateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(channelsImportCreateRequest)
		err := s.ImportChannels(ctx, req.File)
		if err != nil {
			return nil, err
		}
		return channelsImportCreateResponse{}, nil
	}
}

type channelsCreateRequest struct {
	Country string
	Link    string
}

type channelsCreateResponse struct {
}

func makeChannelsCreateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(channelsCreateRequest)
		ch, err := channel.New(req.Country, req.Link)
		if err != nil {
			return nil, err
		}
		_, err = s.CreateChannel(ctx, *ch)
		if err != nil {
			return nil, err
		}

		return channelsCreateResponse{}, nil
	}
}

type channelsShowRequest struct {
	ID int
}

type channelsShowResponse struct {
	Channel *channel.Channel
}

func makeChannelsShowEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(channelsShowRequest)
		ch, err := s.GetChannel(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return channelsShowResponse{
			Channel: ch,
		}, nil
	}
}

type channelsEditRequest struct {
	ID int
}

type channelsEditResponse struct {
	Channel *channel.Channel
}

func makeChannelsEditEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(channelsEditRequest)
		ch, err := s.GetChannel(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return channelsEditResponse{
			Channel: ch,
		}, nil
	}
}

type channelsUpdateRequest struct {
	ID      int
	Country string
}

type channelsUpdateResponse struct {
}

func makeChannelsUpdateEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(channelsUpdateRequest)
		err := s.UpdateChannel(ctx, req.ID, req.Country)
		if err != nil {
			return nil, err
		}

		return channelsUpdateResponse{}, nil
	}
}

type channelsDeleteRequest struct {
	ID int
}

type channelsDeleteResponse struct {
}

func makeChannelsDeleteEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(channelsDeleteRequest)
		err := s.DeleteChannel(ctx, req.ID)
		if err != nil {
			return nil, err
		}

		return channelsDeleteResponse{}, nil
	}
}
