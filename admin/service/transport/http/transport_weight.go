package http

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func decodeWeightsIndexRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return weightsIndexRequest{}, nil
}

func encodeWeightsIndexResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("weights/index.html")
	res := response.(weightsIndexResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Weights,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeWeightsNewRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return weightsNewRequest{}, nil
}

func encodeWeightsNewResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("weights/new.html")
	_ = response.(weightsNewResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: nil,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeWeightsCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}
	p, _ := strconv.Atoi(r.PostForm.Get("percentage"))
	return weightsCreateRequest{
		Type:       r.PostForm.Get("type"),
		Percentage: p,
	}, nil
}

func encodeWeightsCreateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(weightsCreateResponse)

	w.Header().Set("Location", fmt.Sprintf("/weights"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}

func decodeWeightsEditRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return weightsEditRequest{
		ID: ID,
	}, nil
}

func encodeWeightsEditResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	t := getTemplate("weights/edit.html")
	res := response.(weightsEditResponse)

	b := bytes.Buffer{}
	err := t.Execute(&b, Data{
		Data: res.Weight,
	})
	if err != nil {
		return err
	}

	io.Copy(w, &b)

	return nil
}

func decodeWeightsUpdateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	err := r.ParseForm()
	if err != nil {
		return nil, err
	}

	vars := mux.Vars(r)

	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}

	p, _ := strconv.Atoi(r.PostForm.Get("percentage"))
	return weightsUpdateRequest{
		ID:         ID,
		Type:       r.PostForm.Get("type"),
		Percentage: p,
	}, nil
}

func encodeWeightsUpdateResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	_ = response.(weightsUpdateResponse)

	w.Header().Set("Location", fmt.Sprintf("/weights"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}

func decodeWeightsDeleteRequest(_ context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	ID, err := strconv.Atoi(vars["id"])
	if err != nil {
		return nil, err
	}
	return weightsDeleteRequest{
		ID: ID,
	}, nil
}

func encodeWeightsDeleteResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	//t := getTemplate("weights/index.html")
	//_ = response.(weightsDeleteResponse)

	//b := bytes.Buffer{}
	//err := t.Execute(&b, Data{
	//Data: nil,
	//})
	//if err != nil {
	//return err
	//}

	w.Header().Set("Location", fmt.Sprintf("/weights"))
	w.WriteHeader(http.StatusSeeOther)

	return nil
}
