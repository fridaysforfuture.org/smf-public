package http

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/xescugc/smf/admin/service"
)

type homeRequest struct {
}

type homeResponse struct {
}

func makeHomeEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(homeRequest)

		return homeResponse{}, nil
	}
}
