package service

import (
	"context"

	"github.com/xescugc/smf/interops/tier"
	"golang.org/x/xerrors"
)

// CreateTier creates a new Tier t
func (a *Admin) CreateTier(ctx context.Context, t tier.Tier) (int, error) {
	if err := t.Validate(); err != nil {
		return 0, xerrors.Errorf("invalid Tier: %w", err)
	}

	tis, err := a.tiers.Filter(ctx)
	if err != nil {
		return 0, xerrors.Errorf("fail to filter Tier: %w", err)
	}

	for _, ti := range tis {
		if !((t.MinChannelFollowersCount < ti.MinChannelFollowersCount && t.MaxChannelFollowersCount < ti.MinChannelFollowersCount) ||
			(t.MinChannelFollowersCount > ti.MaxChannelFollowersCount && t.MaxChannelFollowersCount > ti.MaxChannelFollowersCount)) {
			return 0, xerrors.Errorf("it's overlapping with %q", ti.Name)
		}
	}

	ID, err := a.tiers.Create(ctx, t)
	if err != nil {
		return 0, xerrors.Errorf("fail to create Tier %+v: %w", t, err)
	}

	return ID, nil
}

// GetTiers returns all the Tiers
func (a *Admin) GetTiers(ctx context.Context) ([]*tier.Tier, error) {
	tis, err := a.tiers.Filter(ctx)
	if err != nil {
		return nil, xerrors.Errorf("fail to filter Tier: %w", err)
	}

	return tis, nil
}

// GetTier returns the Tier with ID
func (a *Admin) GetTier(ctx context.Context, ID int) (*tier.Tier, error) {
	ti, err := a.tiers.Find(ctx, ID)
	if err != nil {
		return nil, xerrors.Errorf("fail to find Tier: %w", err)
	}

	return ti, nil
}

// UpdateTier updates the Tier with ID with Tier t
func (a *Admin) UpdateTier(ctx context.Context, ID int, t tier.Tier) error {
	if err := t.Validate(); err != nil {
		return xerrors.Errorf("invalid Tier: %w", err)
	}

	err := a.tiers.Update(ctx, ID, t)
	if err != nil {
		return xerrors.Errorf("fail to update Tier with ID %d %+v: %w", ID, t, err)
	}

	return nil
}

// DeleteTier deletes the Tier with ID
func (a *Admin) DeleteTier(ctx context.Context, ID int) error {
	err := a.tiers.Delete(ctx, ID)
	if err != nil {
		return xerrors.Errorf("fail to delete Tier with ID %d: %w", ID, err)
	}

	return nil
}
