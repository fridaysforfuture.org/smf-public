package service_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/interops/weight"
)

func TestGetPosts(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			p   = post.Post{
				Title: "hi",
			}
		)
		defer s.Finish()

		s.Posts.EXPECT().Filter(ctx).Return([]*post.Post{&p}, nil)

		ps, err := s.S.GetPosts(ctx)
		require.NoError(t, err)
		assert.Equal(t, []*post.Post{&p}, ps)
	})
}

func TestGetPostsBySystems(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			p   = post.WithChannel{
				Post: post.Post{
					Title: "hi",
				},
			}
			w = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&w}, nil)
		s.Posts.EXPECT().FilterBySystems(ctx, []*weight.Weight{&w}).Return([]*post.WithChannel{&p}, nil)

		ps, err := s.S.GetPostsBySystems(ctx)
		require.NoError(t, err)
		assert.Equal(t, []*post.WithChannel{&p}, ps)
	})
}

func TestGetPost(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			pid = 2
			p   = post.Post{
				ID:    pid,
				Title: "hi",
			}
		)
		defer s.Finish()

		s.Posts.EXPECT().Find(ctx, pid).Return(&p, nil)

		dbp, err := s.S.GetPost(ctx, pid)
		require.NoError(t, err)
		assert.Equal(t, &p, dbp)
	})
}

func TestDisablePost(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			pid = 2
			p   = post.Post{
				ID:      pid,
				Title:   "hi",
				Enabled: true,
			}
			pu = post.Post{
				ID:      pid,
				Title:   "hi",
				Enabled: false,
			}
		)
		defer s.Finish()

		s.Posts.EXPECT().Find(ctx, pid).Return(&p, nil)
		s.Posts.EXPECT().Update(ctx, pid, pu).Return(nil)

		err := s.S.DisablePost(ctx, pid)
		require.NoError(t, err)
	})
}
