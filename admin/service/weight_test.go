package service_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/weight"
)

func TestCreateWeight(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			wID = 2
			w   = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&w}, nil)
		s.Weights.EXPECT().Create(ctx, w).Return(wID, nil)

		ID, err := s.S.CreateWeight(ctx, w)
		require.NoError(t, err)
		assert.Equal(t, wID, ID)
	})
	t.Run("ErrorMortePercentage", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			w   = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
			dbw = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 99,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&dbw}, nil)

		ID, err := s.S.CreateWeight(ctx, w)
		assert.Empty(t, ID)
		assert.EqualError(t, err, "the total Percentage of the Weights can not be higher than 100")
	})
}

func TestGetWeights(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			w   = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
		)

		defer s.Finish()
		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&w}, nil)

		ws, err := s.S.GetWeights(ctx)
		require.NoError(t, err)
		assert.Equal(t, []*weight.Weight{&w}, ws)
	})
}

func TestGetWeight(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			wID = 2
			w   = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Find(ctx, wID).Return(&w, nil)

		we, err := s.S.GetWeight(ctx, wID)
		require.NoError(t, err)
		assert.Equal(t, &w, we)
	})
}

func TestUpdateWeight(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			wID = 2
			w   = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&w}, nil)
		s.Weights.EXPECT().Update(ctx, wID, w).Return(nil)

		err := s.S.UpdateWeight(ctx, wID, w)
		require.NoError(t, err)
	})
	t.Run("SuccessUpdatingSame", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			wID = 2
			w   = weight.Weight{
				ID:         wID,
				Type:       channel.Twitter,
				Percentage: 10,
			}
			dbw = weight.Weight{
				ID:         wID,
				Type:       channel.Twitter,
				Percentage: 99,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&dbw}, nil)
		s.Weights.EXPECT().Update(ctx, wID, w).Return(nil)

		err := s.S.UpdateWeight(ctx, wID, w)
		require.NoError(t, err)
	})
	t.Run("ErrorMortePercentage", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			wID = 2
			w   = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
			dbw = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 99,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&dbw}, nil)

		err := s.S.UpdateWeight(ctx, wID, w)
		assert.EqualError(t, err, "the total Percentage of the Weights can not be higher than 100")
	})
}

func TestDeleteWeight(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			wID = 2
		)
		defer s.Finish()

		s.Weights.EXPECT().Delete(ctx, wID).Return(nil)

		err := s.S.DeleteWeight(ctx, wID)
		require.NoError(t, err)
	})
}
