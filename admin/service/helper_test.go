package service_test

import (
	"context"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/golang/mock/gomock"
	"github.com/xescugc/smf/admin/service"
	imock "github.com/xescugc/smf/interops/mock"
	wmock "github.com/xescugc/smf/worker/mock"
)

type Service struct {
	Posts    *imock.PostRepository
	Channels *imock.ChannelRepository
	Tiers    *imock.TierRepository
	Weights  *imock.WeightRepository
	Worker   *wmock.Service

	S service.Service

	Ctrl *gomock.Controller
}

func NewService(t *testing.T) Service {
	ctrl := gomock.NewController(t)

	cr := imock.NewChannelRepository(ctrl)
	pr := imock.NewPostRepository(ctrl)
	tr := imock.NewTierRepository(ctrl)
	wr := imock.NewWeightRepository(ctrl)
	w := wmock.NewService(ctrl)
	ctx := context.Background()
	logger := log.NewNopLogger()

	a := service.New(ctx, cr, pr, tr, wr, w, logger)

	return Service{
		Posts:    pr,
		Channels: cr,
		Tiers:    tr,
		Weights:  wr,
		Worker:   w,

		S: a,

		Ctrl: ctrl,
	}
}

func (s *Service) Finish() {
	s.Ctrl.Finish()
}
