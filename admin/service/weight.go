package service

import (
	"context"

	"github.com/xescugc/smf/interops/weight"
	"golang.org/x/xerrors"
)

// CreateWeight creates a new Weight w
func (a *Admin) CreateWeight(ctx context.Context, w weight.Weight) (int, error) {
	if err := w.Validate(); err != nil {
		return 0, xerrors.Errorf("invalid Weight: %w", err)
	}

	ws, err := a.weights.Filter(ctx)
	if err != nil {
		return 0, xerrors.Errorf("fail to filter Weight: %w", err)
	}

	total := 0
	for _, iw := range ws {
		total += iw.Percentage
	}
	total += w.Percentage

	if total > 100 {
		return 0, xerrors.Errorf("the total Percentage of the Weights can not be higher than 100")
	}

	ID, err := a.weights.Create(ctx, w)
	if err != nil {
		return 0, xerrors.Errorf("fail to create Weight %+v: %w", w, err)
	}

	return ID, nil
}

// GetWeights retuns all the Weights
func (a *Admin) GetWeights(ctx context.Context) ([]*weight.Weight, error) {
	ws, err := a.weights.Filter(ctx)
	if err != nil {
		return nil, xerrors.Errorf("fail to filter Weight: %w", err)
	}

	return ws, nil
}

// GetWeight returns the Weight with ID
func (a *Admin) GetWeight(ctx context.Context, ID int) (*weight.Weight, error) {
	w, err := a.weights.Find(ctx, ID)
	if err != nil {
		return nil, xerrors.Errorf("fail to find Weight: %w", err)
	}

	return w, nil
}

// UpdateWeight updates the Weight with ID with Weight w
func (a *Admin) UpdateWeight(ctx context.Context, ID int, w weight.Weight) error {
	if err := w.Validate(); err != nil {
		return xerrors.Errorf("invalid Weight: %w", err)
	}

	ws, err := a.weights.Filter(ctx)
	if err != nil {
		return xerrors.Errorf("fail to filter Weight: %w", err)
	}

	total := 0
	for _, iw := range ws {
		// The one we are updating we skip it
		if iw.ID == ID {
			continue
		}
		total += iw.Percentage
	}
	total += w.Percentage

	if total > 100 {
		return xerrors.Errorf("the total Percentage of the Weights can not be higher than 100")
	}

	err = a.weights.Update(ctx, ID, w)
	if err != nil {
		return xerrors.Errorf("fail to update Weight with ID %d %+v: %w", ID, w, err)
	}

	return nil
}

// DeleteWeight deletes the Weight with ID
func (a *Admin) DeleteWeight(ctx context.Context, ID int) error {
	err := a.weights.Delete(ctx, ID)
	if err != nil {
		return xerrors.Errorf("fail to delete Weight with ID %d: %w", ID, err)
	}

	return nil
}
