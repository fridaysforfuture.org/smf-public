package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"

	faktory "github.com/contribsys/faktory/client"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/handlers"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"github.com/xescugc/smf/admin/assets"
	"github.com/xescugc/smf/admin/config"
	"github.com/xescugc/smf/admin/service"
	transportHTTP "github.com/xescugc/smf/admin/service/transport/http"
	"github.com/xescugc/smf/interops/mysql"
	"github.com/xescugc/smf/interops/mysql/migrate"
	fkClient "github.com/xescugc/smf/worker/client/faktory"
)

func init() {
	flag.String("db-host", "", "Database Host")
	flag.Int("db-port", 0, "Database Port")
	flag.String("db-user", "", "Database User")
	flag.String("db-password", "", "Database Password")
	flag.String("db-name", "", "Database Name")
	flag.Int("port", 0, "Service Port")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Println("To specify the URL for the 'faktory' you have to do it via FAKTORY_URL or FAKTORY_PROVIDER (https://github.com/contribsys/faktory_worker_go#faq)")
	}

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
}

func main() {
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	// Initialize and validate the config
	var cfg config.Config
	if err := viper.Unmarshal(&cfg); err != nil {
		panic(err)
	}

	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	// Initialize the Logger
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "service", "admin", "caller", log.DefaultCaller)

	// Initializing Faktory
	logger.Log("msg", "Faktory starting ...")
	fkc, err := faktory.Open()
	if err != nil {
		panic(err)
	}
	c := fkClient.New(fkc)
	logger.Log("msg", "Faktory started")

	// Initializing DB
	logger.Log("msg", "MariaDB starting ...")
	db, err := mysql.New(viper.GetString("db-host"), viper.GetInt("db-port"), viper.GetString("db-user"), viper.GetString("db-password"), mysql.Options{
		DBName:          viper.GetString("db-name"),
		MultiStatements: true,
		ClientFoundRows: true,
	})
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "MariaDB started")

	logger.Log("msg", "migrations running ...")
	err = migrate.Migrate(db)
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "migrations finished")

	// Initializint Repositories
	cr := mysql.NewChannelRepository(db)
	pr := mysql.NewPostRepository(db)
	tr := mysql.NewTierRepository(db)
	wr := mysql.NewWeightRepository(db)

	ctx, cancelCtx := context.WithCancel(context.Background())
	defer cancelCtx()
	// Initialize the Service
	s := service.New(ctx, cr, pr, tr, wr, c, logger)

	mux := http.NewServeMux()

	mux.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(assets.AssetFS())))

	mux.Handle("/", transportHTTP.MakeHandler(s))

	http.Handle("/", handlers.LoggingHandler(os.Stdout, mux))

	logger.Log("msg", "HTTP", "addr", cfg.Port)
	logger.Log("err", http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), nil))
}
