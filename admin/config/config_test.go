package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xescugc/smf/admin/config"
)

func TestValidate(t *testing.T) {
	tests := []struct {
		Name string
		Cfg  config.Config
		Err  string
	}{
		{
			Name: "Success",
			Cfg: config.Config{
				Port:       8080,
				DBHost:     "host",
				DBPort:     8080,
				DBUser:     "user",
				DBPassword: "pass",
				DBName:     "name",
			},
		},
		{
			Name: "ErrDBHost",
			Cfg:  config.Config{},
			Err:  "DBHost is required",
		},
		{
			Name: "ErrDBPort",
			Cfg: config.Config{
				DBHost: "host",
			},
			Err: "DBPort is required",
		},
		{
			Name: "ErrDBUser",
			Cfg: config.Config{
				DBHost: "host",
				DBPort: 8080,
			},
			Err: "DBUser is required",
		},
		{
			Name: "ErrDBPassword",
			Cfg: config.Config{
				DBHost: "host",
				DBPort: 8080,
				DBUser: "user",
			},
			Err: "DBPassword is required",
		},
		{
			Name: "ErrDBName",
			Cfg: config.Config{
				DBHost:     "host",
				DBPort:     8080,
				DBUser:     "user",
				DBPassword: "pass",
			},
			Err: "DBName is required",
		},
		{
			Name: "ErrDBName",
			Cfg: config.Config{
				DBHost:     "host",
				DBPort:     8080,
				DBUser:     "user",
				DBPassword: "pass",
			},
			Err: "DBName is required",
		},
		{
			Name: "ErrPort",
			Cfg: config.Config{
				DBHost:     "host",
				DBPort:     8080,
				DBUser:     "user",
				DBPassword: "pass",
				DBName:     "name",
			},
			Err: "Port is required",
		},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			err := tt.Cfg.Validate()
			if err != nil {
				assert.Equal(t, tt.Err, err.Error())
			} else if tt.Err != "" {
				assert.Equal(t, tt.Err, "")
			}
		})
	}
}
