package config

import "golang.org/x/xerrors"

// Config provides the strucutre for the
// Configuration of the Service
type Config struct {
	Port int `mapstructure:"port"`

	DBHost     string `mapstructure:"db-host"`
	DBPort     int    `mapstructure:"db-port"`
	DBUser     string `mapstructure:"db-user"`
	DBPassword string `mapstructure:"db-password"`
	DBName     string `mapstructure:"db-name"`
}

// Validate validates the Config
func (c *Config) Validate() error {
	if c.DBHost == "" {
		return xerrors.New("DBHost is required")
	} else if c.DBPort == 0 {
		return xerrors.New("DBPort is required")
	} else if c.DBUser == "" {
		return xerrors.New("DBUser is required")
	} else if c.DBPassword == "" {
		return xerrors.New("DBPassword is required")
	} else if c.DBName == "" {
		return xerrors.New("DBName is required")
	} else if c.Port == 0 {
		return xerrors.New("Port is required")
	}

	return nil
}
