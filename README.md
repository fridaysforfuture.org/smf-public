# Social Media Feed (SMF)

This is a project for the FFF.

## Project definition

Vocabulary:

* Channel: We understand as Channel a social media URL/handler/username (depending on the Type: IG, FB, YT, TW) in which we'll read the Posts from. One Channel is only of 1 Type. It'll have also the Country in which it belongs too.
* Posts: The data created on to the Channels that will be listed.

From each Channel (https://fridaysforfuture.org/about) we'll fetch and store the last 2 months (current and past) Posts. The Posts will be sorted by Timestamp and Posts included to the main list will be filtered by a Tier (Channels) and the Weight System (% base over the top Posts of a Channel Type YT, IG, TW, FB). The endpoint format will be in RSS/JSON and will support filtering (TBD).

The data for each Channel will be fetched and updated every day to see if a Channel has to change Tier and if a Posts passes the "Cut" to be on the main list. The list of Channels will also be keep up to date with the https://fridaysforfuture.org/about.

We'll not keep any history of the Posts and once they expire (2months old) they'll be deleted.

The Tier System (TS) will define Tiers of Channels based on Followers/Subscribers count and a threshold of Likes/(Shares OR Comments) for a Post to be included to the main list. This TS will be modifiable from the Admin Page. Ex:

* Tier 1: 50k followers. (100 Shares OR 50 Comments) AND 500 Likes
* Tier 2: 10k-49k followers. (50 Shares OR 40 Comments) AND 250 Likes
* Tier 3: 1k-10k followers. (20 Shares OR 5 Comments) AND 100 Likes

The Weight System (WS) will define the total % of Posts for each Channel Type that will be on the main list. It'll be modifiable from the Admin page. Ex:
* 50% IG, 20% TW, 20% FB, 10% YT.

Admin Page (AP) will be a way to interact with the Posts and Channels. Will be able to CRUD Channels (Create/Read/Update/Delete) and Create/Delete-Disable/Boost Posts (Boost still TBD). Will also allow to modify the WS and TS.

## Architecture

It has 3 services:

* Admin: Which is on the `admin/` dir is used to modify the platform and add new things to it.
* Public: Which is on the `public/` dir is used to be exposed to the internet for public use to fetch
the public Feed (RSS/JSON)
* Worker: Which is on the `worker/` dir is used to fetch the infromation of the Channels
and Posts from the specific APIs (Twitter, Instagram ...) and basically deal with
any type of background job

Other technologies used are:

* MariaDB as main database
* Faktory as queue system

All this services are started using `docker-compose` and it's definition is inside the
`docker/` folder. There we can also find the Dockerfiles for each service and the
different compose files for each environment.

Each service has it's own `main.go` so they can be runned independently, the configuration parameters can be defined via flags (`--flag`) or ENV (`FLAG`)
and they'll fail if any of the configuration parameters is missing.
As it's GO we can create binaries directly from the code as everything is embeded inside the conde even the templates and assets from the Admin service.

On the Admin service there are 2 goroutines (background jobs) that each hour they try to refetch Channels older than 24h and delete Posts older the 2 months.

## Development

We have a `make test` to run all the tests and a `make generate` to generate the code. To run the migrations we have `make migrate` which will drop and recreate the DB. If we want to serve all the services use `make serve` and if not we have one Makefile target for each service `make serve-*`

## Secret Keys

All the secret keys are inside a `.env` file which is not pushed to GIT and Docker knows how to read and intermpolate to the config.

To get the credentials for each type of Channel you can do it from:

* Instagram: https://developers.facebook.com/docs/instagram-api/getting-started
* Twitter: https://developer.twitter.com/en/docs/labs/filtered-stream/quick-start
* YouTube: https://developers.google.com/youtube/v3/getting-started
