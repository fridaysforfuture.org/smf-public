// Package youtube provides a wrapper for the YouTube API
// Docs on YT API:
// * https://developers.google.com/youtube/v3/docs/channels/list
// * https://godoc.org/google.golang.org/api/youtube/v3#ChannelsService
// * https://godoc.org/google.golang.org/api/youtube/v3#Channel
// * https://godoc.org/google.golang.org/api/youtube/v3#ChannelStatistics
// * https://godoc.org/google.golang.org/api/youtube/v3#hdr-Other_authentication_options
package youtube

import (
	"context"
	"fmt"
	"time"

	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/post"
	"golang.org/x/xerrors"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
)

const (
	channelPart = "statistics,id,snippet"
	searchPart  = "snippet"
	videoPart   = "statistics,snippet"
)

// YouTube is the struct that holds the YouTube information
type YouTube struct {
	client *youtube.Service
}

// New initializes a new YouTube that fulfills the fetcher.Fetcher interface
// with the key
func New(key string) (*YouTube, error) {
	ys, err := youtube.NewService(context.Background(), option.WithAPIKey(key))
	if err != nil {
		return nil, xerrors.Errorf("can not initialize YouTube Fetcher with key %s: %w", key, err)
	}
	return &YouTube{
		client: ys,
	}, nil
}

// Type returns the channel.Type
func (y *YouTube) Type() channel.Type { return channel.YouTube }

// Channel returns the channel from the SourceHandler or SourceID
func (y *YouTube) Channel(ctx context.Context, c channel.Channel) (*channel.Channel, error) {
	var (
		clr *youtube.ChannelListResponse
		err error
	)
	if c.SourceID != "" {
		clr, err = y.client.Channels.List(channelPart).Context(ctx).Id(c.SourceID).Do()
		if err != nil {
			err = xerrors.Errorf("could not get Channel by SourceID %s: %w", c.SourceID, err)
		}
	} else {
		clr, err = y.client.Channels.List(channelPart).Context(ctx).ForUsername(c.SourceHandler).Do()
		if err != nil {
			err = xerrors.Errorf("could not get Channel by SourceHandler %s: %w", c.SourceHandler, err)
		}
	}
	if err != nil {
		return nil, err
	}
	if len(clr.Items) == 0 {
		return nil, xerrors.Errorf("could not find Channel %+v: %w", c, errors.NotFound)
	}

	i := clr.Items[0]

	c.FollowersCount = int(i.Statistics.SubscriberCount)
	c.Name = i.Snippet.Title
	c.ImageURL = i.Snippet.Thumbnails.Default.Url
	c.SourceID = i.Id

	return &c, nil
}

func (y *YouTube) Posts(ctx context.Context, c channel.Channel) ([]*post.Post, error) {
	ps := make([]*post.Post, 0)
	afterTime := time.Now().Add(-(30 * 24 * 2) * time.Hour)
	err := y.client.Search.List(searchPart).Context(ctx).
		ChannelId(c.SourceID).EventType("completed").Order("date").
		Type("video").PublishedAfter(afterTime.Format(time.RFC3339)).
		Pages(ctx, func(slr *youtube.SearchListResponse) error {
			for _, sr := range slr.Items {
				vlr, err := y.client.Videos.List(videoPart).Context(ctx).Id(sr.Id.VideoId).Do()
				if err != nil {
					return xerrors.Errorf("could not List for Videos of Channel %+v with ID %s: %w", c, sr.Id.VideoId, err)
				}
				for _, v := range vlr.Items {
					pa, err := time.Parse(time.RFC3339, v.Snippet.PublishedAt)
					if err != nil {
						return xerrors.Errorf("could not Parse time %q: %w", v.Snippet.PublishedAt, err)
					}
					ps = append(ps, &post.Post{
						SourceID:      sr.Id.VideoId,
						Title:         sr.Snippet.Title,
						Link:          fmt.Sprintf("https://www.youtube.com/watch?v=%s", sr.Id.VideoId),
						Description:   v.Snippet.Description,
						PublishedAt:   pa,
						ImagesURLs:    []string{v.Snippet.Thumbnails.High.Url},
						LikesCount:    int(v.Statistics.LikeCount),
						CommentsCount: int(v.Statistics.CommentCount),
						// We have no SharesCount on YT
						SharesCount: 0,
					})
				}
			}
			return nil
		})
	if err != nil {
		return nil, xerrors.Errorf("could not Search for Videos of Channel %+v: %w", c, err)
	}

	return ps, nil
}
