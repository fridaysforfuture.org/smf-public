package config_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xescugc/smf/worker/config"
)

func TestValidate(t *testing.T) {
	tests := []struct {
		Name string
		Cfg  config.Config
		Err  string
	}{
		{
			Name: "Success",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
				DBHost:         "host",
				DBPort:         8080,
				DBUser:         "user",
				DBPassword:     "pass",
				DBName:         "name",
			},
		},
		{
			Name: "ErrTWClientID",
			Cfg:  config.Config{},
			Err:  "TWClientID is required",
		},
		{
			Name: "ErrTWClientID",
			Cfg: config.Config{
				TWClientID: "client",
			},
			Err: "TWClientSecret is required",
		},
		{
			Name: "ErrDBHost",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
			},
			Err: "YTKey is required",
		},
		{
			Name: "ErrDBHost",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
			},
			Err: "DBHost is required",
		},
		{
			Name: "ErrDBPort",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
				DBHost:         "host",
			},
			Err: "DBPort is required",
		},
		{
			Name: "ErrDBUser",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
				DBHost:         "host",
				DBPort:         8080,
			},
			Err: "DBUser is required",
		},
		{
			Name: "ErrDBPassword",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
				DBHost:         "host",
				DBPort:         8080,
				DBUser:         "user",
			},
			Err: "DBPassword is required",
		},
		{
			Name: "ErrDBName",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
				DBHost:         "host",
				DBPort:         8080,
				DBUser:         "user",
				DBPassword:     "pass",
			},
			Err: "DBName is required",
		},
		{
			Name: "ErrDBName",
			Cfg: config.Config{
				TWClientID:     "client",
				TWClientSecret: "secret",
				YTKey:          "key",
				DBHost:         "host",
				DBPort:         8080,
				DBUser:         "user",
				DBPassword:     "pass",
			},
			Err: "DBName is required",
		},
	}
	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			err := tt.Cfg.Validate()
			if err != nil {
				assert.Equal(t, tt.Err, err.Error())
			} else if tt.Err != "" {
				assert.Equal(t, tt.Err, "")
			}
		})
	}
}
