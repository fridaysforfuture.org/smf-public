package twitter

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/post"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/clientcredentials"
	"golang.org/x/xerrors"
)

// Twitter is the struct that holds the Twitter information
type Twitter struct {
	client *twitter.Client
}

// New initializes a new Twitter that fulfills the fetcher.Fetcher interface
// with the clientID and clientSecret
func New(clientID, clientSecret string) *Twitter {
	config := &clientcredentials.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		TokenURL:     "https://api.twitter.com/oauth2/token",
	}

	// http.Client will automatically authorize Requests
	httpClient := config.Client(oauth2.NoContext)
	// Twitter client
	client := twitter.NewClient(httpClient)

	return &Twitter{
		client: client,
	}
}

// Type returns the channel.Type
func (t *Twitter) Type() channel.Type { return channel.Twitter }

// Channel returns the channel from the c.SourceHandler
func (t *Twitter) Channel(ctx context.Context, c channel.Channel) (*channel.Channel, error) {
	user, _, err := t.client.Users.Show(&twitter.UserShowParams{
		ScreenName: c.SourceHandler,
	})
	if err != nil {
		return nil, xerrors.Errorf("could not Show Users from Twitter with handler %q: %w", c.SourceHandler, err)
	}

	c.FollowersCount = user.FollowersCount
	c.FriendsCount = user.FriendsCount
	c.SourceID = user.IDStr
	c.ImageURL = user.ProfileImageURLHttps
	c.Name = user.Name

	return &c, nil
}

// Posts returns the Posts from the c.SourceHandler
func (t *Twitter) Posts(ctx context.Context, c channel.Channel) ([]*post.Post, error) {
	// We average a month to 30 days
	afterTime := time.Now().Add(-(30 * 24 * 2) * time.Hour)
	var (
		isEndDate bool
		maxID     int
		posts     []*post.Post
	)
	for !isEndDate {
		ps, err := t.posts(ctx, c, maxID)
		if err != nil {
			return nil, err
		}
		if len(ps) == 0 {
			isEndDate = true
			continue
		}
		posts = append(posts, ps...)
		lastP := ps[len(ps)-1]
		if lastP.PublishedAt.After(afterTime) {
			maxID, _ = strconv.Atoi(lastP.SourceID)
		} else {
			isEndDate = true
		}
	}

	return posts, nil
}

// posts is a helper to handle the fetch from the Twitter API and deal with the specific
// error it may return
func (t *Twitter) posts(ctx context.Context, c channel.Channel, maxID int) ([]*post.Post, error) {
	twetts, resp, err := t.client.Timelines.UserTimeline(&twitter.UserTimelineParams{
		ExcludeReplies:  twitter.Bool(false),
		IncludeRetweets: twitter.Bool(false),
		ScreenName:      c.SourceHandler,
		MaxID:           int64(maxID),
	})

	if err != nil {
		if twerrs, ok := err.(twitter.APIError); ok {
			// Iterate over all the possible errors and only
			// care about the 88, if not just pass for now
			for _, twerr := range twerrs.Errors {
				// RateLimit code https://developer.twitter.com/en/docs/basics/rate-limiting.html
				if twerr.Code == 88 {
					rl, err := strconv.Atoi(resp.Header.Get("x-rate-limit-reset"))
					if err != nil {
						return nil, xerrors.Errorf("invalid format x-rate-limit-reset with error: %w", err)
					}
					// Calculate the Duration to wait and
					// add a more Minute just to be sure
					d := time.Until(time.Unix(int64(rl), 0).Add(time.Minute))
					// We are not gonna block the worker
					// as it already has a retry mecanism we'll just throw an
					// error with information about the time
					return nil, xerrors.Errorf("Reached the Rate Limit from Twitter. It'll be resetted in %s", d)
				}
			}
		}
		return nil, xerrors.Errorf("could not Show Users from Twitter with handler %q: %w", c.SourceHandler, err)
	}

	posts := make([]*post.Post, 0, len(twetts))
	for _, tw := range twetts {
		pa, err := time.Parse(time.RubyDate, tw.CreatedAt)
		if err != nil {
			return nil, xerrors.Errorf("could not Parse time %q: %w", tw.CreatedAt, err)
		}
		images := make([]string, 0)
		for _, u := range tw.Entities.Media {
			images = append(images, u.MediaURLHttps)
		}

		var description string
		if tw.Truncated {
			if tw.ExtendedTweet == nil {
				description = tw.Text
			} else {
				description = tw.ExtendedTweet.FullText
			}
		} else {
			description = tw.Text
		}

		posts = append(posts, &post.Post{
			SourceID:      tw.IDStr,
			Title:         tw.Text,
			Link:          fmt.Sprintf("https://twitter.com/%s/status/%s", tw.User.ScreenName, tw.IDStr),
			Description:   description,
			PublishedAt:   pa,
			ImagesURLs:    images,
			LikesCount:    tw.FavoriteCount,
			CommentsCount: tw.ReplyCount,
			SharesCount:   tw.RetweetCount,
		})
	}

	return posts, nil
}
