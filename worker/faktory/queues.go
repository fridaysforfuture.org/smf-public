package faktory

//go:generate stringer -type Queue

// Queue is a type to define the queues
type Queue int

// List of all the available queues
const (
	FetchChannelQueue Queue = iota
	UpdateChannelQueue
	FetchPostsQueue
	UpsertPostQueue
)
