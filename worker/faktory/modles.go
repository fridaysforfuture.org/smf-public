package faktory

import (
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/post"
)

// FetchChannelModel is the model when using the FetchChannel
type FetchChannelModel struct {
	ID int `json:"id"`
}

// UpdateChannelModel is the model when using the UpdateChannel
type UpdateChannelModel struct {
	ID      int             `json:"id"`
	Channel channel.Channel `json:"channel"`
}

// FetchPostsModel is the model when using the FetchPosts
type FetchPostsModel struct {
	ID int `json:"id"`
}

// UpsertPostModel is the model when using the UpsertPost
type UpsertPostModel struct {
	ID   int       `json:"id"`
	Post post.Post `json:"post"`
}
