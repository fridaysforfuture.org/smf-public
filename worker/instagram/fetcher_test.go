package instagram_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/worker/instagram"
)

func TestPosts(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		igf := instagram.New()
		c := channel.Channel{
			SourceHandler: "madxef",
		}
		posts, err := igf.Posts(context.Background(), c)
		require.NoError(t, err)
		assert.True(t, len(posts) > 0)
	})
}
