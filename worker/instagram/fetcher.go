package instagram

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/post"
	"golang.org/x/xerrors"
)

type Instagram struct {
	client *http.Client
}

func New() *Instagram {
	return &Instagram{
		client: &http.Client{
			Timeout: time.Second * 10,
		},
	}
}

// Type returns the channel.Type
func (i *Instagram) Type() channel.Type { return channel.Instagram }

// Channel returns the channel from the c.SourceHandler
func (i *Instagram) Channel(ctx context.Context, c channel.Channel) (*channel.Channel, error) {
	res, err := i.client.Get(fmt.Sprintf("https://www.instagram.com/%s/?__a=1", c.SourceHandler))
	if err != nil {
		return nil, xerrors.Errorf("could not Get Users from Instagram with handler %q: %w", c.SourceHandler, err)
	}
	defer res.Body.Close()

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, xerrors.Errorf("could not read Users body from Instagram with handler %q and body %s: %w", c.SourceHandler, string(b), err)
	}

	var u User
	err = json.Unmarshal(b, &u)
	if err != nil {
		if bytes.Contains(b, []byte("The link you followed may be broken, or the page may have been removed.")) {
			return nil, xerrors.Errorf("could not find Instagram user %s", c.SourceHandler, ierrors.NotFound)
		}
		return nil, xerrors.Errorf("could not decode Users from Instagram with handler %q and JSON %s: %w", c.SourceHandler, string(b), err)
	}

	c.FollowersCount = int(u.Graphql.User.EdgeFollowedBy.Count)
	c.FriendsCount = int(u.Graphql.User.EdgeFollow.Count)
	c.SourceID = u.Graphql.User.ID
	c.ImageURL = u.Graphql.User.ProfilePicURLHd
	c.Name = u.Graphql.User.FullName

	return &c, nil
}

// Posts returns the Posts from the c.SourceHandler
func (i *Instagram) Posts(ctx context.Context, c channel.Channel) ([]*post.Post, error) {
	res, err := i.client.Get(fmt.Sprintf("https://www.instagram.com/%s/?__a=1", c.SourceHandler))
	if err != nil {
		return nil, xerrors.Errorf("could not Get Users from Instagram with handler %q: %w", c.SourceHandler, err)
	}
	defer res.Body.Close()

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, xerrors.Errorf("could not read Users body from Instagram with handler %q and body %s: %w", c.SourceHandler, string(b), err)
	}

	// We average a month to 30 days
	afterTime := time.Now().Add(-(30 * 24 * 2) * time.Hour)

	var u User
	err = json.Unmarshal(b, &u)
	if err != nil {
		if bytes.Contains(b, []byte("The link you followed may be broken, or the page may have been removed.")) {
			return nil, xerrors.Errorf("could not find Instagram user %s", c.SourceHandler, ierrors.NotFound)
		}
		return nil, xerrors.Errorf("could not decode Users from Instagram with handler %q and JSON %s: %w", c.SourceHandler, string(b), err)
	}

	posts := make([]*post.Post, 0, u.Graphql.User.EdgeOwnerToTimelineMedia.Count)

	for _, e := range u.Graphql.User.EdgeOwnerToTimelineMedia.Edges {
		n := e.Node
		images := make([]string, 0)
		text := ""
		if n.Typename == "GraphSidecar" {
			for _, ei := range n.EdgeSidecarToChildren.Edges {
				n := ei.Node
				images = append(images, n.DisplayURL)
			}
		} else if n.Typename == "GraphImage" || n.Typename == "GraphVideo" {
			images = append(images, n.ThumbnailSrc)
		}

		if len(n.EdgeMediaToCaption.Edges) > 0 {
			text = n.EdgeMediaToCaption.Edges[0].Node.Text
		}

		p := &post.Post{
			SourceID:    n.ID,
			Title:       text,
			Link:        fmt.Sprintf("https://www.instagram.com/p/%s", n.Shortcode),
			Description: text,
			PublishedAt: time.Unix(int64(n.TakenAtTimestamp), 0),
			ImagesURLs:  images,
		}

		if !p.PublishedAt.After(afterTime) {
			continue
		}

		if n.Typename == "GraphSidecar" {
			res, err = i.client.Get(fmt.Sprintf("%s/?__a=1", p.Link))
			if err != nil {
				return nil, xerrors.Errorf("could not Get Media from Instagram with handler %q for link %s: %w", c.SourceHandler, p.Link, err)
			}
			defer res.Body.Close()

			var m Media
			err = json.Unmarshal(b, &m)
			if err != nil {
				if bytes.Contains(b, []byte("The link you followed may be broken, or the page may have been removed.")) {
					return nil, xerrors.Errorf("could not find Instagram Media for user %s with link %s: %w", c.SourceHandler, p.Link, ierrors.NotFound)
				}
				return nil, xerrors.Errorf("could not decode Media from Instagram with handler %q and link %s: %w", c.SourceHandler, p.Link, err)
			}

			p.LikesCount = m.Graphql.ShortcodeMedia.EdgeMediaPreviewLike.Count
			p.CommentsCount = m.Graphql.ShortcodeMedia.EdgeMediaPreviewComment.Count
		} else {
			p.LikesCount = n.EdgeMediaPreviewLike.Count
			p.CommentsCount = n.EdgeMediaToComment.Count
		}

		posts = append(posts, p)
	}

	return posts, nil
}
