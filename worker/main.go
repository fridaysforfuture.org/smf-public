package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	faktory "github.com/contribsys/faktory/client"
	worker "github.com/contribsys/faktory_worker_go"
	"github.com/go-kit/kit/log"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"github.com/xescugc/smf/interops/mysql"
	"github.com/xescugc/smf/interops/mysql/migrate"
	fkClient "github.com/xescugc/smf/worker/client/faktory"
	"github.com/xescugc/smf/worker/config"
	"github.com/xescugc/smf/worker/fetcher"
	"github.com/xescugc/smf/worker/instagram"
	"github.com/xescugc/smf/worker/service"
	fkTransport "github.com/xescugc/smf/worker/service/transport/faktory"
	"github.com/xescugc/smf/worker/twitter"
	"github.com/xescugc/smf/worker/youtube"
)

func init() {
	flag.String("tw-client-id", "", "Define the Twitter ClientID")
	flag.String("tw-client-secret", "", "Define the Twitter ClientSecret")

	flag.String("yt-key", "", "Define the YouTube API Key")

	flag.String("db-host", "", "Database Host")
	flag.Int("db-port", 0, "Database Port")
	flag.String("db-user", "", "Database User")
	flag.String("db-password", "", "Database Password")
	flag.String("db-name", "", "Database Name")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Println("To specify the URL for the 'faktory' you have to do it via FAKTORY_URL or FAKTORY_PROVIDER (https://github.com/contribsys/faktory_worker_go#faq)")
	}

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
}

func main() {
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	// Initialize and validate the config
	var cfg config.Config
	if err := viper.Unmarshal(&cfg); err != nil {
		panic(err)
	}

	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	// Initialize the Logger
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "service", "worker", "caller", log.DefaultCaller)

	// Initializing Fetchers
	twf := twitter.New(cfg.TWClientID, cfg.TWClientSecret)
	ytf, err := youtube.New(cfg.YTKey)
	if err != nil {
		panic(err)
	}
	igf := instagram.New()

	// Intializing Fetchers Factory
	ff, err := fetcher.NewFactory(twf, ytf, igf)
	if err != nil {
		panic(err)
	}

	// Initializing Faktory
	logger.Log("msg", "Faktory starting ...")
	fkc, err := faktory.Open()
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "Faktory started")
	c := fkClient.New(fkc)

	// Initializing DB
	logger.Log("msg", "MariaDB starting ...")
	db, err := mysql.New(viper.GetString("db-host"), viper.GetInt("db-port"), viper.GetString("db-user"), viper.GetString("db-password"), mysql.Options{
		DBName:          viper.GetString("db-name"),
		MultiStatements: true,
		ClientFoundRows: true,
	})
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "MariaDB started")

	logger.Log("msg", "migrations running ...")
	err = migrate.Migrate(db)
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "migrations finished")
	// Initializint Repositories
	cr := mysql.NewChannelRepository(db)
	pr := mysql.NewPostRepository(db)

	// Initialize the Service
	s := service.New(cr, pr, ff, c)

	// Initialize the Worker
	mgr := worker.NewManager()
	fkTransport.MakeConnection(mgr, s, logger)

	logger.Log("msg", "connection started")

	mgr.Run()
}
