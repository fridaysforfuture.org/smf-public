package faktory

import (
	"context"
	"encoding/json"

	"github.com/contribsys/faktory/client"
	worker "github.com/contribsys/faktory_worker_go"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/worker/faktory"
	"golang.org/x/xerrors"
)

// Client is an implementation of a Client for the Worker Service
// using Faktory as transport
type Client struct {
	pool worker.Pool
}

// New returns a new Client
func New(conn *client.Client) *Client {
	pool, err := worker.NewChannelPool(0, 5, func() (worker.Closeable, error) { return client.Open() })
	if err != nil {
		panic(err)
	}
	return &Client{
		pool: pool,
	}
}

// FetchChannel fetches the Channel with ID
func (c *Client) FetchChannel(ctx context.Context, ID int) error {
	m := faktory.FetchChannelModel{
		ID: ID,
	}

	err := c.push(ctx, faktory.FetchChannelQueue, m)
	if err != nil {
		return xerrors.Errorf("could not push FetchChannel: %w", err)
	}

	return nil
}

// UpdateChannel updates the Channel with ID and Channel ch
func (c *Client) UpdateChannel(ctx context.Context, ID int, ch channel.Channel) error {
	m := faktory.UpdateChannelModel{
		ID:      ID,
		Channel: ch,
	}

	err := c.push(ctx, faktory.UpdateChannelQueue, m)
	if err != nil {
		return xerrors.Errorf("could not push UpdateChannel: %w", err)
	}

	return nil
}

// FetchPosts fetches all the Posts for the Channel ID cID
func (c *Client) FetchPosts(ctx context.Context, cID int) error {
	m := faktory.FetchPostsModel{
		ID: cID,
	}

	err := c.push(ctx, faktory.FetchPostsQueue, m)
	if err != nil {
		return xerrors.Errorf("could not push FetchPosts: %w", err)
	}

	return nil
}

// UpsertPost creates or updates the Post p with Channel ID cID
func (c *Client) UpsertPost(ctx context.Context, cID int, p post.Post) error {
	m := faktory.UpsertPostModel{
		ID:   cID,
		Post: p,
	}

	err := c.push(ctx, faktory.UpsertPostQueue, m)
	if err != nil {
		return xerrors.Errorf("could not push UpsertPost: %w", err)
	}

	return nil
}

// push helper function to abstract how to push data to Faktory queues
// by specifying the q and the data
func (c *Client) push(ctx context.Context, q faktory.Queue, data interface{}) error {
	b, err := json.Marshal(data)
	if err != nil {
		return xerrors.Errorf("could not marshal %+v: %w", data, err)
	}

	job := client.NewJob(q.String(), string(b))
	conn, err := c.pool.Get()
	if err != nil {
		return xerrors.Errorf("could not get a connection from the pool: %w", err)
	}

	pc := conn.(*worker.PoolConn)
	f, ok := pc.Closeable.(*client.Client)
	if !ok {
		return xerrors.New("could not convert pc.Closeable to client.Client")
	}
	defer f.Close()

	err = f.Push(job)
	if err != nil {
		return xerrors.Errorf("could not push to %q the data %+v: %w", q.String(), data, err)
	}

	return nil
}
