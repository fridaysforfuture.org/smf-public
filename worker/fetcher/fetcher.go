package fetcher

import (
	"context"

	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/post"
	"golang.org/x/xerrors"
)

//go:generate mockgen -destination=../mock/fetcher.go -mock_names=Fetcher=Fetcher -package mock github.com/xescugc/smf/worker/fetcher Fetcher
//go:generate mockgen -destination=../mock/fetcher_factory.go -mock_names=Factory=FetcherFactory -package mock github.com/xescugc/smf/worker/fetcher Factory

// Fetcher is the interface for the Fetcher of every specific channel.Type
type Fetcher interface {
	// Type returns the specific type of the Fetcher
	Type() channel.Type

	// Channel refreshes the c information
	Channel(ctx context.Context, c channel.Channel) (*channel.Channel, error)

	// Posts returns all the c Posts
	// TODO: Update this to return a chan so
	// it can be returned while still fetching
	// to improve the speed
	Posts(ctx context.Context, c channel.Channel) ([]*post.Post, error)
}

// Factory is a factory of Fetchers
type Factory interface {
	// Get returns the specific Fetcher from the channel.Type
	Get(channel.Type) (Fetcher, error)
}

type factory struct {
	fetchers map[channel.Type]Fetcher
}

// NewFactory initializes the Factory with the Fetchers
func NewFactory(fes ...Fetcher) (Factory, error) {
	fa := &factory{
		fetchers: make(map[channel.Type]Fetcher),
	}

	for _, fe := range fes {
		if _, ok := fa.fetchers[fe.Type()]; ok {
			return nil, xerrors.Errorf("channel type %q repeated", fe.Type())
		}
		fa.fetchers[fe.Type()] = fe
	}

	return fa, nil
}

func (f *factory) Get(t channel.Type) (Fetcher, error) {
	fe, ok := f.fetchers[t]
	if !ok {
		return nil, xerrors.Errorf("channel type %q not found", t)
	}
	return fe, nil
}
