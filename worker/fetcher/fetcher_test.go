package fetcher_test

import (
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/worker/fetcher"
	"github.com/xescugc/smf/worker/mock"
)

func TestNewFactory(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		tw := mock.NewFetcher(ctrl)

		tw.EXPECT().Type().Return(channel.Twitter).Times(2)

		_, err := fetcher.NewFactory(tw)
		require.NoError(t, err)
	})
	t.Run("Error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		tw := mock.NewFetcher(ctrl)
		tw2 := mock.NewFetcher(ctrl)

		tw.EXPECT().Type().Return(channel.Twitter).Times(2)
		tw2.EXPECT().Type().Return(channel.Twitter).Times(2)

		_, err := fetcher.NewFactory(tw, tw2)
		assert.Error(t, err, "channel type 'twitter' repeated")
	})
}

func TestFactory_Get(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		tw := mock.NewFetcher(ctrl)

		tw.EXPECT().Type().Return(channel.Twitter).Times(2)

		fa, err := fetcher.NewFactory(tw)
		require.NoError(t, err)

		ef, err := fa.Get(channel.Twitter)
		require.NoError(t, err)
		assert.Equal(t, tw, ef)
	})

	t.Run("Error", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		tw := mock.NewFetcher(ctrl)

		tw.EXPECT().Type().Return(channel.Twitter).Times(2)

		fa, err := fetcher.NewFactory(tw)
		require.NoError(t, err)

		ef, err := fa.Get(channel.Instagram)
		assert.Error(t, err, "channel type  'instagram' not found")
		assert.Nil(t, ef)
	})
}
