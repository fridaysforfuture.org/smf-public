package service_test

import (
	"testing"

	"github.com/golang/mock/gomock"
	imock "github.com/xescugc/smf/interops/mock"
	"github.com/xescugc/smf/worker/mock"
	"github.com/xescugc/smf/worker/service"
)

type Service struct {
	Posts    *imock.PostRepository
	Channels *imock.ChannelRepository
	Fetchers *mock.FetcherFactory
	Worker   *mock.Service

	S service.Service

	Ctrl *gomock.Controller
}

func NewService(t *testing.T) Service {
	ctrl := gomock.NewController(t)

	cr := imock.NewChannelRepository(ctrl)
	pr := imock.NewPostRepository(ctrl)
	fe := mock.NewFetcherFactory(ctrl)
	svc := mock.NewService(ctrl)

	w := service.New(cr, pr, fe, svc)

	return Service{
		Posts:    pr,
		Channels: cr,
		Fetchers: fe,
		Worker:   svc,

		S: w,

		Ctrl: ctrl,
	}
}

func (s *Service) Finish() {
	s.Ctrl.Finish()
}
