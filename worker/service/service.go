package service

import (
	"context"
	"time"

	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/worker/fetcher"
	"golang.org/x/xerrors"
)

//go:generate mockgen -destination=../mock/service.go -mock_names=Service=Service -package mock github.com/xescugc/smf/worker/service Service

// Service is the main interface of the Worker service
// with all the functions provided
type Service interface {
	FetchChannel(ctx context.Context, ID int) error
	UpdateChannel(ctx context.Context, ID int, c channel.Channel) error
	FetchPosts(ctx context.Context, cID int) error
	UpsertPost(ctx context.Context, cID int, p post.Post) error
}

// Worker is a struct that implements the Service
type Worker struct {
	channels channel.Repository
	posts    post.Repository
	fetchers fetcher.Factory
	worker   Service
}

// New returns a new Worker which implements the Service interface
func New(cr channel.Repository, pr post.Repository, fe fetcher.Factory, w Service) *Worker {
	return &Worker{
		channels: cr,
		posts:    pr,
		fetchers: fe,
		worker:   w,
	}
}

// FetchChannel fetches the Channel with ID
func (w *Worker) FetchChannel(ctx context.Context, ID int) error {
	c, err := w.channels.Find(ctx, ID)
	if err != nil {
		return xerrors.Errorf("could not Find Channel %d: %w", ID, err)
	}

	f, err := w.fetchers.Get(c.Type)
	if err != nil {
		return xerrors.Errorf("could not Get Fetcher %q: %w", c.Type, err)
	}

	c, err = f.Channel(ctx, *c)
	if err != nil {
		return xerrors.Errorf("could not Fetch Channel: %w", err)
	}

	c.LastRefreshedAt = time.Now()

	err = w.worker.UpdateChannel(ctx, ID, *c)
	if err != nil {
		return xerrors.Errorf("could not UpdateChannel: %w", err)
	}

	return nil
}

// UpdateChannel updates the Channel with ID and Channel ch
func (w *Worker) UpdateChannel(ctx context.Context, ID int, c channel.Channel) error {
	// TODO: For now we just replace, but we may concider at some point
	// to also check some fields when the admin
	// is able to change some fields
	err := w.channels.Update(ctx, ID, c)
	if err != nil {
		return xerrors.Errorf("could not Update Channel: %w", err)
	}

	return nil
}

// FetchPosts fetches all the Posts for the Channel ID cID
func (w *Worker) FetchPosts(ctx context.Context, cID int) error {
	c, err := w.channels.Find(ctx, cID)
	if err != nil {
		return xerrors.Errorf("could not Find Channel %d: %w", cID, err)
	}

	f, err := w.fetchers.Get(c.Type)
	if err != nil {
		return xerrors.Errorf("could not Get Fetcher %q: %w", c.Type, err)
	}

	ps, err := f.Posts(ctx, *c)
	if err != nil {
		return xerrors.Errorf("could not Fetch Channel: %w", err)
	}

	for _, p := range ps {
		err = w.worker.UpsertPost(ctx, cID, *p)
		if err != nil {
			return xerrors.Errorf("could not UpsertPost: %w", err)
		}
	}

	return nil
}

// UpsertPost creates or updates the Post p with Channel ID cID
func (w *Worker) UpsertPost(ctx context.Context, cID int, p post.Post) error {
	dbp, err := w.posts.FindBySourceID(ctx, cID, p.SourceID)
	if err != nil && !xerrors.Is(err, ierrors.NotFound) {
		return xerrors.Errorf("could not FindBySourceID: %w", err)
	}

	// If no dbp then we Create the Post
	if dbp == nil {
		// When we create them we always set it as true
		p.Enabled = true
		_, err = w.posts.Create(ctx, cID, p)
		if err != nil {
			return xerrors.Errorf("could not Create Post: %w", err)
		}
		return nil
	}

	// We pass through the Enabled value
	// so we do not override it
	p.Enabled = dbp.Enabled

	// Else we Update the already existing Post
	err = w.posts.Update(ctx, dbp.ID, p)
	if err != nil {
		return xerrors.Errorf("could not Update Post: %w", err)
	}

	return nil
}
