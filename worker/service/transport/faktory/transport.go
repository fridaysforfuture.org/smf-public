package faktory

import (
	"encoding/json"
	"fmt"

	"github.com/go-kit/kit/log"

	worker "github.com/contribsys/faktory_worker_go"
	"github.com/xescugc/smf/worker/faktory"
	"github.com/xescugc/smf/worker/service"
)

// MakeConnection starts the worker and listens to the queues and calls the
// corresponding function on the Service s
func MakeConnection(conn *worker.Manager, s service.Service, logger log.Logger) {
	conn.Register(faktory.FetchChannelQueue.String(), func(ctx worker.Context, args ...interface{}) error {
		var m faktory.FetchChannelModel
		err := json.Unmarshal([]byte(args[0].(string)), &m)
		if err != nil {
			return err
		}
		logger.Log("queue", faktory.FetchChannelQueue.String(), "args", fmt.Sprintf("%+v", m))
		err = s.FetchChannel(ctx, m.ID)
		if err != nil {
			return err
		}

		return nil
	})

	conn.Register(faktory.UpdateChannelQueue.String(), func(ctx worker.Context, args ...interface{}) error {
		var m faktory.UpdateChannelModel
		err := json.Unmarshal([]byte(args[0].(string)), &m)
		if err != nil {
			return err
		}
		logger.Log("queue", faktory.UpdateChannelQueue.String(), "args", fmt.Sprintf("%+v", m))
		err = s.UpdateChannel(ctx, m.ID, m.Channel)
		if err != nil {
			return err
		}

		return nil
	})

	conn.Register(faktory.FetchPostsQueue.String(), func(ctx worker.Context, args ...interface{}) error {
		var m faktory.FetchPostsModel
		err := json.Unmarshal([]byte(args[0].(string)), &m)
		if err != nil {
			return err
		}
		logger.Log("queue", faktory.FetchPostsQueue.String(), "args", fmt.Sprintf("%+v", m))
		err = s.FetchPosts(ctx, m.ID)
		if err != nil {
			return err
		}

		return nil
	})

	conn.Register(faktory.UpsertPostQueue.String(), func(ctx worker.Context, args ...interface{}) error {
		var m faktory.UpsertPostModel
		err := json.Unmarshal([]byte(args[0].(string)), &m)
		if err != nil {
			return err
		}
		logger.Log("queue", faktory.UpsertPostQueue.String(), "args", fmt.Sprintf("%+v", m))
		err = s.UpsertPost(ctx, m.ID, m.Post)
		if err != nil {
			return err
		}

		return nil
	})
}
