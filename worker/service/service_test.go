package service_test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/xescugc/smf/interops/channel"
	ierrors "github.com/xescugc/smf/interops/errors"
	imock "github.com/xescugc/smf/interops/mock"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/worker/mock"
	"github.com/xescugc/smf/worker/service"
)

func TestNew(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		cr := imock.NewChannelRepository(ctrl)
		pr := imock.NewPostRepository(ctrl)
		fe := mock.NewFetcherFactory(ctrl)
		svc := mock.NewService(ctrl)

		w := service.New(cr, pr, fe, svc)
		assert.NotNil(t, w)
		assert.Implements(t, (*service.Service)(nil), w)
	})
}

func TestFetchChannel(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			cID = 2
			ch  = channel.Channel{
				ID:             cID,
				Type:           channel.Twitter,
				FollowersCount: 10,
			}
			nch = channel.Channel{
				Type:           channel.Twitter,
				FollowersCount: 20,
			}
			ctx = context.Background()
			f   = mock.NewFetcher(s.Ctrl)
		)
		defer s.Finish()

		s.Channels.EXPECT().Find(ctx, cID).Return(&ch, nil)
		s.Fetchers.EXPECT().Get(ch.Type).Return(f, nil)
		f.EXPECT().Channel(ctx, ch).Return(&nch, nil)
		s.Worker.EXPECT().UpdateChannel(ctx, cID, gomock.Any()).Do(func(_ context.Context, _ int, c channel.Channel) {
			assert.NotZero(t, c.LastRefreshedAt)
			nch.LastRefreshedAt = c.LastRefreshedAt
			assert.Equal(t, nch, c)
		}).Return(nil)

		err := s.S.FetchChannel(ctx, cID)
		require.NoError(t, err)
	})
}

func TestUpdateChannel(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			cID = 2
			ch  = channel.Channel{
				ID:             cID,
				Type:           channel.Twitter,
				FollowersCount: 10,
			}
			ctx = context.Background()
		)
		defer s.Finish()

		s.Channels.EXPECT().Update(ctx, cID, ch).Return(nil)

		err := s.S.UpdateChannel(ctx, cID, ch)
		require.NoError(t, err)
	})
}

func TestFetchPosts(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			cID = 2
			ch  = channel.Channel{
				ID:             cID,
				Type:           channel.Twitter,
				FollowersCount: 10,
			}
			ps = []*post.Post{
				&post.Post{
					Title: "1",
				},
				&post.Post{
					Title: "2",
				},
			}
			ctx = context.Background()
			f   = mock.NewFetcher(s.Ctrl)
		)
		defer s.Finish()

		s.Channels.EXPECT().Find(ctx, cID).Return(&ch, nil)
		s.Fetchers.EXPECT().Get(ch.Type).Return(f, nil)
		f.EXPECT().Posts(ctx, ch).Return(ps, nil)
		s.Worker.EXPECT().UpsertPost(ctx, cID, *ps[0])
		s.Worker.EXPECT().UpsertPost(ctx, cID, *ps[1])

		err := s.S.FetchPosts(ctx, cID)
		require.NoError(t, err)
	})
}

func TestUpsertPost(t *testing.T) {
	t.Run("SuccessWithUpdateDisabled", func(t *testing.T) {
		var (
			s   = NewService(t)
			cID = 2
			ctx = context.Background()
			p   = post.Post{
				Title:    "2",
				SourceID: "srcid",
			}
			dbp = post.Post{
				ID:       3,
				Title:    "1",
				SourceID: "srcid",
			}
		)
		defer s.Finish()

		s.Posts.EXPECT().FindBySourceID(ctx, cID, p.SourceID).Return(&dbp, nil)
		s.Posts.EXPECT().Update(ctx, dbp.ID, p).Return(nil)

		err := s.S.UpsertPost(ctx, cID, p)
		require.NoError(t, err)
	})
	t.Run("SuccessWithUpdateEnabled", func(t *testing.T) {
		var (
			s   = NewService(t)
			cID = 2
			ctx = context.Background()
			p   = post.Post{
				Title:    "2",
				SourceID: "srcid",
			}
			pc = post.Post{
				Title:    "2",
				SourceID: "srcid",
				Enabled:  true,
			}
			dbp = post.Post{
				ID:       3,
				Title:    "1",
				SourceID: "srcid",
				Enabled:  true,
			}
		)
		defer s.Finish()

		s.Posts.EXPECT().FindBySourceID(ctx, cID, p.SourceID).Return(&dbp, nil)
		s.Posts.EXPECT().Update(ctx, dbp.ID, pc).Return(nil)

		err := s.S.UpsertPost(ctx, cID, p)
		require.NoError(t, err)
	})
	t.Run("SuccessWithCreate", func(t *testing.T) {
		var (
			s   = NewService(t)
			cID = 2
			ctx = context.Background()
			p   = post.Post{
				Title:    "2",
				SourceID: "srcid",
			}
			pc = post.Post{
				Title:    "2",
				SourceID: "srcid",
				Enabled:  true,
			}
		)
		defer s.Finish()

		s.Posts.EXPECT().FindBySourceID(ctx, cID, p.SourceID).Return(nil, ierrors.NotFound)
		s.Posts.EXPECT().Create(ctx, cID, pc).Return(2, nil)

		err := s.S.UpsertPost(ctx, cID, p)
		require.NoError(t, err)
	})
}
