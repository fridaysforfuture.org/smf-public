package channel

import (
	"net/url"
	"regexp"
	"time"

	"golang.org/x/xerrors"
)

var (
	reTW = regexp.MustCompile(`^https?://(?:www\.)?twitter\.com/(?P<hashtag>.[^/]+)/?$`)
	reIG = regexp.MustCompile(`^https?://(?:www\.)?instagram\.com/(?P<hashtag>.[^/]+)/?$`)

	reYTID   = regexp.MustCompile(`^https?://(?:www\.)?youtube\.com/channel/(?P<ID>.[^/]+)/?$`)
	reYTUser = regexp.MustCompile(`^https?://(?:www\.)?youtube\.com/(?:user/)?(?P<hashtag>.[^/]+)/?$`)
)

// Channel is the struct that defines the Channel data
type Channel struct {
	ID int

	Link string
	// Country is not validated as it's used just for the admin
	// and not intended to be returned to the public as it
	// may contain wrong data
	Country string
	Type    Type

	SourceID      string
	SourceHandler string
	Name          string
	ImageURL      string

	FollowersCount int
	FriendsCount   int

	LastRefreshedAt time.Time
}

// New will initialyze a Channel with the link and country
// validating those values and also setting the Type and
// the SourceHandler from the Link
func New(country, link string) (*Channel, error) {
	_, err := url.Parse(link)
	if err != nil {
		return nil, xerrors.Errorf("could not Parse url %q: %w", link, err)
	}

	c := &Channel{
		Link:    link,
		Country: country,
	}

	if reTW.MatchString(link) {
		c.Type = Twitter
		res := reTW.FindAllStringSubmatch(link, -1)
		c.SourceHandler = res[0][1]
	} else if reIG.MatchString(link) {
		c.Type = Instagram
		res := reIG.FindAllStringSubmatch(link, -1)
		c.SourceHandler = res[0][1]
	} else if reYTID.MatchString(link) {
		c.Type = YouTube
		res := reYTID.FindAllStringSubmatch(link, -1)
		c.SourceID = res[0][1]
	} else if reYTUser.MatchString(link) {
		c.Type = YouTube
		res := reYTUser.FindAllStringSubmatch(link, -1)
		c.SourceHandler = res[0][1]
	} else {
		return nil, xerrors.Errorf("invalid or not supported url %q", link)
	}

	return c, nil
}

// Validate validates the data of a Channel
func (c *Channel) Validate() error {
	if c.Link == "" {
		return xerrors.New("Link is required")
	} else if c.Country == "" {
		return xerrors.New("Country is required")
	} else if c.Type == NoType {
		return xerrors.New("Type is required")
	} else if !c.Type.IsAType() {
		return xerrors.Errorf("Type %q is not a valid Type", c.Type)
	} else if c.SourceHandler == "" && c.SourceID == "" {
		return xerrors.New("SourceHandler or SourceID is required")
	}
	return nil
}
