package channel_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
)

func TestNew(t *testing.T) {
	t.Run("SuccessTW", func(t *testing.T) {
		c, err := channel.New("Spain", "https://twitter.com/xescugc/")
		require.NoError(t, err)
		assert.Equal(t, channel.Twitter, c.Type)
		assert.Equal(t, "xescugc", c.SourceHandler)
	})
	t.Run("SuccessYT", func(t *testing.T) {
		c, err := channel.New("Spain", "https://twitter.com/xescugc/")
		require.NoError(t, err)
		assert.Equal(t, channel.Twitter, c.Type)
		assert.Equal(t, "xescugc", c.SourceHandler)
	})
	t.Run("SuccessIG", func(t *testing.T) {
		c, err := channel.New("Spain", "https://www.instagram.com/fridaysforfuturecuador/")
		require.NoError(t, err)
		assert.Equal(t, channel.Instagram, c.Type)
		assert.Equal(t, "fridaysforfuturecuador", c.SourceHandler)
	})
	t.Run("SuccessYTUser", func(t *testing.T) {
		c, err := channel.New("Spain", "https://www.youtube.com/user/muffinman3000/")
		require.NoError(t, err)
		assert.Equal(t, channel.YouTube, c.Type)
		assert.Equal(t, "muffinman3000", c.SourceHandler)
	})
	t.Run("SuccessYTID", func(t *testing.T) {
		c, err := channel.New("Spain", "https://www.youtube.com/channel/UCbpMy0Fg74eXXkvxJrtEn3w/")
		require.NoError(t, err)
		assert.Equal(t, channel.YouTube, c.Type)
		assert.Equal(t, "UCbpMy0Fg74eXXkvxJrtEn3w", c.SourceID)
	})
	t.Run("ErrorInvalidURLType", func(t *testing.T) {
		c, err := channel.New("Spain", "https://pepito.com/xescugc")
		assert.Nil(t, c)
		assert.EqualError(t, err, `invalid or not supported url "https://pepito.com/xescugc"`)
	})
}

func TestValidate(t *testing.T) {
	tests := []struct {
		Name    string
		Channel channel.Channel
		Err     string
	}{
		{
			Name: "Success",
			Channel: channel.Channel{
				Link:          "https://twitter.com/xescugc",
				Country:       "Spain",
				Type:          channel.Twitter,
				SourceHandler: "xescugc",
			},
		},
		{
			Name: "RequiredLink",
			Channel: channel.Channel{
				Country:       "Spain",
				Type:          channel.Twitter,
				SourceHandler: "xescugc",
			},
			Err: "Link is required",
		},
		{
			Name: "RequiredCountry",
			Channel: channel.Channel{
				Link:          "https://twitter.com/xescugc",
				Type:          channel.Twitter,
				SourceHandler: "xescugc",
			},
			Err: "Country is required",
		},
		{
			Name: "RequiredType",
			Channel: channel.Channel{
				Link:          "https://twitter.com/xescugc",
				Country:       "Spain",
				SourceHandler: "xescugc",
			},
			Err: "Type is required",
		},
		{
			Name: "RequiredValidType",
			Channel: channel.Channel{
				Link:          "https://twitter.com/xescugc",
				Country:       "Spain",
				Type:          channel.Type(99),
				SourceHandler: "xescugc",
			},
			Err: `Type "Type(99)" is not a valid Type`,
		},
		{
			Name: "RequiredShourceHandler",
			Channel: channel.Channel{
				Link:    "https://twitter.com/xescugc",
				Country: "Spain",
				Type:    channel.Twitter,
			},
			Err: "SourceHandler or SourceID is required",
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			err := tt.Channel.Validate()
			if err != nil {
				assert.Equal(t, tt.Err, err.Error())
			} else if tt.Err != "" {
				assert.Equal(t, tt.Err, "")
			}
		})
	}
}
