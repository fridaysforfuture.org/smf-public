package channel

import (
	"context"
	"time"
)

//go:generate mockgen -destination=../mock/channel_repository.go -mock_names=Repository=ChannelRepository -package mock github.com/xescugc/smf/interops/channel Repository

// Repository is the interface of all the action that can be done to a Channel
type Repository interface {
	Create(ctx context.Context, c Channel) (int, error)
	Find(ctx context.Context, ID int) (*Channel, error)
	FindByLink(ctx context.Context, link string) (*Channel, error)
	Filter(ctx context.Context) ([]*Channel, error)
	FilterByLastRefresh(ctx context.Context, t time.Time) ([]*Channel, error)
	Update(ctx context.Context, ID int, c Channel) error
	Delete(ctx context.Context, ID int) error
}
