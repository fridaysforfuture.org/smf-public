package channel

//go:generate enumer -type=Type -transform=snake -output=type_string.go -linecomment=true

// Type is the type for the Types of Channel
type Type int

// List of all the available Types
const (
	NoType Type = iota
	Instagram
	Twitter
	YouTube // youtube
	Facebook
)
