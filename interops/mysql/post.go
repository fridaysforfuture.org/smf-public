package mysql

import (
	"context"
	"database/sql"
	"fmt"
	"math"
	"strings"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/interops/weight"
	"golang.org/x/xerrors"
)

// PostRepository is an implementation of the post.Repository
type PostRepository struct {
	db *sql.DB
}

// dbPost is the sql represenation of the Post
type dbPost struct {
	ID            sql.NullInt64
	SourceID      sql.NullString
	Title         sql.NullString
	Link          sql.NullString
	Description   sql.NullString
	Enabled       sql.NullBool
	ImagesURLs    sql.NullString
	PublishedAt   mysql.NullTime
	LikesCount    sql.NullInt64
	SharesCount   sql.NullInt64
	CommentsCount sql.NullInt64
}

// toDomainEntity transforms the dbPost to a post.Post
func (p dbPost) toDomainEntity() *post.Post {
	imagesURLs := strings.Split(p.ImagesURLs.String, ",")
	return &post.Post{
		ID:            int(p.ID.Int64),
		SourceID:      p.SourceID.String,
		Title:         p.Title.String,
		Link:          p.Link.String,
		Description:   p.Description.String,
		Enabled:       p.Enabled.Bool,
		ImagesURLs:    imagesURLs,
		PublishedAt:   p.PublishedAt.Time,
		LikesCount:    int(p.LikesCount.Int64),
		SharesCount:   int(p.SharesCount.Int64),
		CommentsCount: int(p.CommentsCount.Int64),
	}
}

// NewPostRepository returns a new PostRepository
func NewPostRepository(db *sql.DB) *PostRepository {
	return &PostRepository{
		db: db,
	}
}

// Create creates a Post p with Channel ID cID and returns the ID of the created Post
func (r *PostRepository) Create(ctx context.Context, cID int, p post.Post) (int, error) {
	imagesURLs := strings.Join(p.ImagesURLs, ",")
	res, err := r.db.ExecContext(ctx, `
		INSERT INTO posts (source_id, title, link, description, enabled, images_urls, published_at, likes_count, shares_count, comments_count, channel_id)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`, p.SourceID, p.Title, p.Link, p.Description, p.Enabled, imagesURLs, p.PublishedAt, p.LikesCount, p.SharesCount, p.CommentsCount, cID)
	if err != nil {
		return 0, xerrors.Errorf("error while inserting Post %+v: %w", p, err)
	}

	id, err := lastInsertedID(res)
	if err != nil {
		return 0, xerrors.Errorf("error when getting Post ID %+v: %w", p, err)
	}

	return id, nil
}

// Find returns the Post with ID
func (r *PostRepository) Find(ctx context.Context, ID int) (*post.Post, error) {
	row := r.db.QueryRowContext(ctx, `
		SELECT p.id, p.source_id, p.title, p.link, p.description, p.enabled, p.images_urls, p.published_at, p.likes_count, p.shares_count, p.comments_count
		FROM posts AS p
		WHERE p.id = ? AND p.enabled = TRUE
	`, ID)

	p, err := scanPost(row)
	if err != nil {
		return nil, xerrors.Errorf("failed when scanning Post %d: %w", ID, err)
	}

	return p, nil
}

// FindBySourceID returns the Post with SourceID srcID and ChannelID cID
func (r *PostRepository) FindBySourceID(ctx context.Context, cID int, srcID string) (*post.Post, error) {
	row := r.db.QueryRowContext(ctx, `
		SELECT p.id, p.source_id, p.title, p.link, p.description, p.enabled, p.images_urls, p.published_at, p.likes_count, p.shares_count, p.comments_count
		FROM posts AS p
		JOIN channels AS c
			ON c.id = p.channel_id
		WHERE p.source_id = ? AND p.channel_id = ? AND p.enabled = TRUE
	`, srcID, cID)

	p, err := scanPost(row)
	if err != nil {
		return nil, xerrors.Errorf("failed when scanning Post by SourceID %q: %w", srcID, err)
	}

	return p, nil
}

// Filter returns all the Posts
func (r *PostRepository) Filter(ctx context.Context) ([]*post.Post, error) {
	rows, err := r.db.QueryContext(ctx, `
		SELECT p.id, p.source_id, p.title, p.link, p.description, p.enabled, p.images_urls, p.published_at, p.likes_count, p.shares_count, p.comments_count
		FROM posts AS p
		WHERE p.enabled = TRUE
	`)
	if err != nil {
		return nil, xerrors.Errorf("could not filter Posts: %w", err)
	}
	defer rows.Close()

	cs, err := scanPosts(rows)
	if err != nil {
		return nil, xerrors.Errorf("could not scan Posts: %w", err)
	}

	return cs, nil
}

// FilterBySystems retuns all the Posts filtered by the Weights ws and the Tiers
func (r *PostRepository) FilterBySystems(ctx context.Context, ws []*weight.Weight) ([]*post.WithChannel, error) {
	if len(ws) == 0 {
		return nil, nil
	}
	selects := make([]string, 0, len(ws))
	stpl := `(
			SELECT p.id, p.source_id, p.title, p.link, p.description, p.enabled, p.images_urls, p.published_at, p.likes_count, p.shares_count, p.comments_count,
				c.id AS c_id, c.link AS c_link, c.country AS c_country, c.source_id AS c_source_id,
				c.source_handler AS c_source_handler, c.name AS c_name, c.image_url AS c_image_url,
				c.type AS c_type, c.followers_count AS c_followers_count, c.friends_count AS c_friends_count,
				c.last_refreshed_at AS c_last_refreshed_at
			FROM posts AS p
			JOIN channels AS c
				ON p.channel_id = c.id
			JOIN tiers AS t
				ON c.followers_count > t.min_channel_followers_count && c.followers_count < t.max_channel_followers_count
			JOIN weights AS w
				ON w.type = c.type
			WHERE p.likes_count >= t.post_likes_count AND (p.shares_count >= t.post_shares_count OR p.comments_count >= t.post_comments_count)
				AND w.type = '%s'
				AND p.enabled = TRUE
			ORDER BY p.published_at DESC
			LIMIT %d
		)`
	for _, w := range ws {
		selects = append(selects, fmt.Sprintf(stpl, w.Type.String(), int(math.RoundToEven(((float64(1000)*float64(w.Percentage))/float64(100))))))
	}
	union := strings.Join(selects, " UNION ")
	rows, err := r.db.QueryContext(ctx, fmt.Sprintf(`
		%s
		ORDER BY published_at DESC
	`, union))
	if err != nil {
		return nil, xerrors.Errorf("could not filter Posts: %w", err)
	}
	defer rows.Close()

	cs, err := scanPostsWithChannel(rows)
	if err != nil {
		return nil, xerrors.Errorf("could not scan Posts: %w", err)
	}

	return cs, nil
}

// Update updates the Post ID with Post p
func (r *PostRepository) Update(ctx context.Context, ID int, p post.Post) error {
	imagesURLs := strings.Join(p.ImagesURLs, ",")
	res, err := r.db.ExecContext(ctx, `
		UPDATE posts AS p
		SET p.source_id=?, p.title=?, p.link=?, p.description=?, p.enabled=?, p.images_urls=?, p.published_at=?, p.likes_count=?, p.shares_count=?, p.comments_count=?
		WHERE p.id = ?
	`, p.SourceID, p.Title, p.Link, p.Description, p.Enabled, imagesURLs, p.PublishedAt, p.LikesCount, p.SharesCount, p.CommentsCount, ID)
	if err != nil {
		return xerrors.Errorf("could not update Post %d %+v: %w", ID, p, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not update Post %d %+v: %w", ID, p, err)
	}

	return nil
}

// Delete deletes the Post ID
func (r *PostRepository) Delete(ctx context.Context, ID int) error {
	res, err := r.db.ExecContext(ctx, `
		DELETE p
		FROM posts AS p
		WHERE p.id = ?
	`, ID)
	if err != nil {
		return xerrors.Errorf("could not delete Post %d: %w", ID, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not delete Post %d: %w", ID, err)
	}

	return nil
}

// DeleteByChannelID deletes all the Posts with the Channel ID cID
func (r *PostRepository) DeleteByChannelID(ctx context.Context, cID int) error {
	res, err := r.db.ExecContext(ctx, `
		DELETE p
		FROM posts AS p
		WHERE p.channel_id = ?
	`, cID)
	if err != nil {
		return xerrors.Errorf("could not delete PostByChannelID %d: %w", cID, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not delete PostByChannelID %d: %w", cID, err)
	}

	return nil
}

// DeleteOlderThan deletes all the posts where the PublishedAt is before t
func (r *PostRepository) DeleteOlderThan(ctx context.Context, t time.Time) error {
	res, err := r.db.ExecContext(ctx, `
		DELETE p
		FROM posts AS p
		WHERE p.published_at < ?
	`, t)
	if err != nil {
		return xerrors.Errorf("could not delete Posts OlderThan %s: %w", t, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not delete Posts OlderThan %s: %w", t, err)
	}

	return nil
}

func scanPost(row *sql.Row) (*post.Post, error) {
	var p dbPost

	err := row.Scan(
		&p.ID,
		&p.SourceID,
		&p.Title,
		&p.Link,
		&p.Description,
		&p.Enabled,
		&p.ImagesURLs,
		&p.PublishedAt,
		&p.LikesCount,
		&p.SharesCount,
		&p.CommentsCount,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.NotFound
		}
		return nil, xerrors.Errorf("failed when scanning Post: %w", err)
	}

	return p.toDomainEntity(), nil
}

func scanPosts(rows *sql.Rows) ([]*post.Post, error) {
	var ps []*post.Post

	for rows.Next() {
		var p dbPost

		err := rows.Scan(
			&p.ID,
			&p.SourceID,
			&p.Title,
			&p.Link,
			&p.Description,
			&p.Enabled,
			&p.ImagesURLs,
			&p.PublishedAt,
			&p.LikesCount,
			&p.SharesCount,
			&p.CommentsCount,
		)
		if err != nil {
			return nil, xerrors.Errorf("failed when scanning Post: %w", err)
		}

		ps = append(ps, p.toDomainEntity())
	}

	if err := rows.Err(); err != nil {
		return nil, xerrors.Errorf("failed when scanning Posts: %w", err)
	}

	return ps, nil
}

func scanPostsWithChannel(rows *sql.Rows) ([]*post.WithChannel, error) {
	var ps []*post.WithChannel

	for rows.Next() {
		var p dbPost
		var c dbChannel

		err := rows.Scan(
			&p.ID,
			&p.SourceID,
			&p.Title,
			&p.Link,
			&p.Description,
			&p.Enabled,
			&p.ImagesURLs,
			&p.PublishedAt,
			&p.LikesCount,
			&p.SharesCount,
			&p.CommentsCount,
			&c.ID,
			&c.Link,
			&c.Country,
			&c.SourceID,
			&c.SourceHandler,
			&c.Name,
			&c.ImageURL,
			&c.Type,
			&c.FollowersCount,
			&c.FriendsCount,
			&c.LastRefreshedAt,
		)
		if err != nil {
			return nil, xerrors.Errorf("failed when scanning PostWithChannel: %w", err)
		}

		ps = append(ps, &post.WithChannel{
			Post:    *p.toDomainEntity(),
			Channel: *c.toDomainEntity(),
		})
	}

	if err := rows.Err(); err != nil {
		return nil, xerrors.Errorf("failed when scanning PostsWithChannel: %w", err)
	}

	return ps, nil
}
