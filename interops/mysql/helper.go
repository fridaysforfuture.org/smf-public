package mysql

import (
	"database/sql"

	"github.com/xescugc/smf/interops/errors"
	"golang.org/x/xerrors"
)

// lastInsertedID returns the ID of the Insert
func lastInsertedID(res sql.Result) (int, error) {
	id, err := res.LastInsertId()

	if err != nil {
		return 0, xerrors.Errorf("could not fetch last inserted id: %w", err)
	}

	if id == 0 {
		return 0, xerrors.Errorf("entity was not created")
	}

	return int(id), nil
}

// rowsAffected returns if any row has been affected
// by the query
func rowsAffected(res sql.Result) error {
	n, err := res.RowsAffected()
	if err != nil {
		return xerrors.Errorf("could not get rows affected: %w", err)
	}

	if n == 0 {
		return errors.NotFound
	}

	return nil
}
