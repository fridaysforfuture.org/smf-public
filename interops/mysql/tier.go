package mysql

import (
	"context"
	"database/sql"

	"github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/tier"
	"golang.org/x/xerrors"
)

// TierRepository is an implementation of the tier.Repository
type TierRepository struct {
	db *sql.DB
}

// dbTier is the sql representation of the Tier
type dbTier struct {
	ID                       sql.NullInt64
	Name                     sql.NullString
	MinChannelFollowersCount sql.NullInt64
	MaxChannelFollowersCount sql.NullInt64
	PostLikesCount           sql.NullInt64
	PostSharesCount          sql.NullInt64
	PostCommentsCount        sql.NullInt64
}

// toDomainEntity transforms the dbTier to a tier.Tier
func (t dbTier) toDomainEntity() *tier.Tier {
	return &tier.Tier{
		ID:                       int(t.ID.Int64),
		Name:                     t.Name.String,
		MinChannelFollowersCount: int(t.MinChannelFollowersCount.Int64),
		MaxChannelFollowersCount: int(t.MaxChannelFollowersCount.Int64),
		PostLikesCount:           int(t.PostLikesCount.Int64),
		PostSharesCount:          int(t.PostSharesCount.Int64),
		PostCommentsCount:        int(t.PostCommentsCount.Int64),
	}
}

// NewTierRepository returns a new TierRepository
func NewTierRepository(db *sql.DB) *TierRepository {
	return &TierRepository{
		db: db,
	}
}

// Create creates the Tier t and returns the ID
func (r *TierRepository) Create(ctx context.Context, t tier.Tier) (int, error) {
	res, err := r.db.ExecContext(ctx, `
		INSERT INTO tiers (name, min_channel_followers_count, max_channel_followers_count, post_likes_count, post_shares_count, post_comments_count)
		VALUES (?, ?, ?, ?, ?, ?)
	`, t.Name, t.MinChannelFollowersCount, t.MaxChannelFollowersCount, t.PostLikesCount, t.PostSharesCount, t.PostCommentsCount)
	if err != nil {
		return 0, xerrors.Errorf("error while inserting Tier %+v: %w", t, err)
	}

	id, err := lastInsertedID(res)
	if err != nil {
		return 0, xerrors.Errorf("error when getting Tier ID %+v: %w", t, err)
	}

	return id, nil
}

// Find returns the Tier with the ID
func (r *TierRepository) Find(ctx context.Context, ID int) (*tier.Tier, error) {
	row := r.db.QueryRowContext(ctx, `
		SELECT t.id, t.name, t.min_channel_followers_count, t.max_channel_followers_count, t.post_likes_count, t.post_shares_count, t.post_comments_count
		FROM tiers AS t
		WHERE t.id = ?
	`, ID)

	p, err := scanTier(row)
	if err != nil {
		return nil, xerrors.Errorf("failed when scanning Tier %d: %w", ID, err)
	}

	return p, nil
}

// Filter returns all the Tiers
func (r *TierRepository) Filter(ctx context.Context) ([]*tier.Tier, error) {
	rows, err := r.db.QueryContext(ctx, `
		SELECT t.id, t.name, t.min_channel_followers_count, t.max_channel_followers_count, t.post_likes_count, t.post_shares_count, t.post_comments_count
		FROM tiers AS t
	`)
	if err != nil {
		return nil, xerrors.Errorf("could not filter Tiers: %w", err)
	}
	defer rows.Close()

	ts, err := scanTiers(rows)
	if err != nil {
		return nil, xerrors.Errorf("could not scan Tiers: %w", err)
	}

	return ts, nil
}

// Update updates the Tier with ID with Tier t
func (r *TierRepository) Update(ctx context.Context, ID int, t tier.Tier) error {
	res, err := r.db.ExecContext(ctx, `
		UPDATE tiers AS t
		SET t.name=?, t.min_channel_followers_count=?, t.max_channel_followers_count=?, t.post_likes_count=?, t.post_shares_count=?, t.post_comments_count=?
		WHERE t.id = ?
	`, t.Name, t.MinChannelFollowersCount, t.MaxChannelFollowersCount, t.PostLikesCount, t.PostSharesCount, t.PostCommentsCount, ID)
	if err != nil {
		return xerrors.Errorf("could not update Tier %d %+v: %w", ID, t, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not update Tier %d %+v: %w", ID, t, err)
	}

	return nil
}

// Delete deltes the Tier with ID
func (r *TierRepository) Delete(ctx context.Context, ID int) error {
	res, err := r.db.ExecContext(ctx, `
		DELETE t
		FROM tiers AS t
		WHERE t.id = ?
	`, ID)
	if err != nil {
		return xerrors.Errorf("could not delete Tier %d: %w", ID, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not delete Tier %d: %w", ID, err)
	}

	return nil
}

func scanTier(row *sql.Row) (*tier.Tier, error) {
	var t dbTier

	err := row.Scan(
		&t.ID,
		&t.Name,
		&t.MinChannelFollowersCount,
		&t.MaxChannelFollowersCount,
		&t.PostLikesCount,
		&t.PostSharesCount,
		&t.PostCommentsCount,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.NotFound
		}
		return nil, xerrors.Errorf("failed when scanning Tier: %w", err)
	}

	return t.toDomainEntity(), nil
}

func scanTiers(rows *sql.Rows) ([]*tier.Tier, error) {
	var ts []*tier.Tier

	for rows.Next() {
		var t dbTier

		err := rows.Scan(
			&t.ID,
			&t.Name,
			&t.MinChannelFollowersCount,
			&t.MaxChannelFollowersCount,
			&t.PostLikesCount,
			&t.PostSharesCount,
			&t.PostCommentsCount,
		)
		if err != nil {
			return nil, xerrors.Errorf("failed when scanning Tier: %w", err)
		}

		ts = append(ts, t.toDomainEntity())
	}

	if err := rows.Err(); err != nil {
		return nil, xerrors.Errorf("failed when scanning Tier: %w", err)
	}

	return ts, nil
}
