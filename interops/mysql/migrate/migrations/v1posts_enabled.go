package migrations

// V1PostEnabled adds the field 'enabled' to the Posts
var V1PostEnabled = Migration{
	Name: "PostsEnabled",
	SQL: `
		ALTER TABLE posts
			ADD enabled BOOL NOT NULL DEFAULT TRUE;
	`,
}
