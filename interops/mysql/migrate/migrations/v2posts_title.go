package migrations

// V2PostEnabled adds the field 'enabled' to the Posts
var V2PostsTitle = Migration{
	Name: "PostsEnabled",
	SQL: `
		ALTER TABLE posts
			MODIFY COLUMN title TEXT NOT NULL;
	`,
}
