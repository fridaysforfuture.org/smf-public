package migrations

// V0Initial is the first migration
var V0Initial = Migration{
	Name: "Initial",
	SQL: `
		SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

		CREATE TABLE channels (
			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			link VARCHAR(255) NOT NULL,
			country VARCHAR(50) NOT NULL,
			source_id VARCHAR(50) NOT NULL,
			source_handler VARCHAR(150) NOT NULL,
			name VARCHAR(255),
			image_url TEXT,
			type VARCHAR(30) NOT NULL,
			followers_count INT UNSIGNED,
			friends_count INT UNSIGNED,
			last_refreshed_at TIMESTAMP,

			UNIQUE(link)
		);

		CREATE TABLE posts (
			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			source_id VARCHAR(50) NOT NULL,
			title VARCHAR(255) NOT NULL,
			link VARCHAR(255) NOT NULL UNIQUE,
			description TEXT NOT NULL,
			images_urls TEXT NOT NULL,
			published_at TIMESTAMP,
			likes_count INT UNSIGNED,
			shares_count INT UNSIGNED,
			comments_count INT UNSIGNED,
			channel_id INT UNSIGNED NOT NULL,

			CONSTRAINT fk__posts__channels
				FOREIGN KEY (channel_id) REFERENCES channels (id)
					ON DELETE RESTRICT
					ON UPDATE RESTRICT
		);

		CREATE TABLE tiers (
			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(255) NOT NULL,
			max_channel_followers_count INT UNSIGNED,
			min_channel_followers_count INT UNSIGNED,
			post_likes_count INT UNSIGNED,
			post_shares_count INT UNSIGNED,
			post_comments_count INT UNSIGNED,

			UNIQUE(name)
		);

		CREATE TABLE weights (
			id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
			type VARCHAR(30) NOT NULL,
			percentage INT NOT NULL,

			UNIQUE(type)
		);
	`,
}
