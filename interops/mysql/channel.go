package mysql

import (
	"context"
	"database/sql"
	"time"

	"github.com/go-sql-driver/mysql"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/errors"
	"golang.org/x/xerrors"
)

// ChannelRepository is the implementation of the channel.Repository
type ChannelRepository struct {
	db *sql.DB
}

// dbChannel is the sql representation of the Channel
type dbChannel struct {
	ID              sql.NullInt64
	Link            sql.NullString
	Country         sql.NullString
	SourceID        sql.NullString
	SourceHandler   sql.NullString
	Name            sql.NullString
	ImageURL        sql.NullString
	Type            sql.NullString
	FollowersCount  sql.NullInt64
	FriendsCount    sql.NullInt64
	LastRefreshedAt mysql.NullTime
}

// toDomainEntity transforms the dbChannel to a channel.Channel
func (c dbChannel) toDomainEntity() *channel.Channel {
	t, _ := channel.TypeString(c.Type.String)
	return &channel.Channel{
		ID:              int(c.ID.Int64),
		Link:            c.Link.String,
		Country:         c.Country.String,
		SourceID:        c.SourceID.String,
		SourceHandler:   c.SourceHandler.String,
		Name:            c.Name.String,
		ImageURL:        c.ImageURL.String,
		Type:            t,
		FollowersCount:  int(c.FollowersCount.Int64),
		FriendsCount:    int(c.FriendsCount.Int64),
		LastRefreshedAt: c.LastRefreshedAt.Time,
	}
}

// NewChannelRepository retuns a new ChannelRepository
func NewChannelRepository(db *sql.DB) *ChannelRepository {
	return &ChannelRepository{
		db: db,
	}
}

// Create creates a new Channel c and returns the ID
func (r *ChannelRepository) Create(ctx context.Context, c channel.Channel) (int, error) {
	res, err := r.db.ExecContext(ctx, `
		INSERT INTO channels (link, country, source_id, source_handler, name, image_url, type, followers_count, friends_count, last_refreshed_at)
		VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
	`, c.Link, c.Country, c.SourceID, c.SourceHandler, c.Name, c.ImageURL, c.Type.String(), c.FollowersCount, c.FriendsCount, c.LastRefreshedAt)
	if err != nil {
		return 0, xerrors.Errorf("error while inserting Channel %+v: %w", c, err)
	}

	id, err := lastInsertedID(res)
	if err != nil {
		return 0, xerrors.Errorf("error when getting Channel ID %+v: %w", c, err)
	}

	return id, nil
}

// Find returns the Channel with ID
func (r *ChannelRepository) Find(ctx context.Context, ID int) (*channel.Channel, error) {
	row := r.db.QueryRowContext(ctx, `
		SELECT c.id, c.link, c.country, c.source_id, c.source_handler, c.name, c.image_url, c.type, c.followers_count, c.friends_count, c.last_refreshed_at
		FROM channels AS c
		WHERE c.id = ?
	`, ID)

	c, err := scanChannel(row)
	if err != nil {
		return nil, xerrors.Errorf("failed when scanning Channel %d: %w", ID, err)
	}

	return c, nil
}

// FindByLink returns the Channel with ID
func (r *ChannelRepository) FindByLink(ctx context.Context, link string) (*channel.Channel, error) {
	row := r.db.QueryRowContext(ctx, `
		SELECT c.id, c.link, c.country, c.source_id, c.source_handler, c.name, c.image_url, c.type, c.followers_count, c.friends_count, c.last_refreshed_at
		FROM channels AS c
		WHERE c.link = ?
	`, link)

	c, err := scanChannel(row)
	if err != nil {
		return nil, xerrors.Errorf("failed when scanning Channel %d: %w", link, err)
	}

	return c, nil
}

// Filter returns all the Channels
func (r *ChannelRepository) Filter(ctx context.Context) ([]*channel.Channel, error) {
	rows, err := r.db.QueryContext(ctx, `
		SELECT c.id, c.link, c.country, c.source_id, c.source_handler, c.name, c.image_url, c.type, c.followers_count, c.friends_count, c.last_refreshed_at
		FROM channels AS c
	`)
	if err != nil {
		return nil, xerrors.Errorf("could not filter Channels: %w", err)
	}
	defer rows.Close()

	cs, err := scanChannels(rows)
	if err != nil {
		return nil, xerrors.Errorf("could not scan Channels: %w", err)
	}

	return cs, nil
}

// FilterByLastRefresh returns all the Channels in which the LastRefreshedAt is before t
func (r *ChannelRepository) FilterByLastRefresh(ctx context.Context, t time.Time) ([]*channel.Channel, error) {
	rows, err := r.db.QueryContext(ctx, `
		SELECT c.id, c.link, c.country, c.source_id, c.source_handler, c.name, c.image_url, c.type, c.followers_count, c.friends_count, c.last_refreshed_at
		FROM channels AS c
		WHERE c.last_refreshed_at < ?
	`, t)
	if err != nil {
		return nil, xerrors.Errorf("could not filter Channels by LastRefresh: %w", err)
	}
	defer rows.Close()

	cs, err := scanChannels(rows)
	if err != nil {
		return nil, xerrors.Errorf("could not scan Channels: %w", err)
	}

	return cs, nil
}

// Update updates the Channel with ID with Channel c
func (r *ChannelRepository) Update(ctx context.Context, ID int, c channel.Channel) error {
	res, err := r.db.ExecContext(ctx, `
		UPDATE channels AS c
		SET c.link=?, c.country=?, c.source_id=?, c.source_handler=?, c.name=?, c.image_url=?, c.type=?, c.followers_count=?, c.friends_count=?, c.last_refreshed_at=?
		WHERE c.id = ?
	`, c.Link, c.Country, c.SourceID, c.SourceHandler, c.Name, c.ImageURL, c.Type.String(), c.FollowersCount, c.FriendsCount, c.LastRefreshedAt, ID)
	if err != nil {
		return xerrors.Errorf("could not update Channel %d %+v: %w", ID, c, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not update Channel %d %+v: %w", ID, c, err)
	}

	return nil
}

// Delete deletes the Channel with ID
func (r *ChannelRepository) Delete(ctx context.Context, ID int) error {
	res, err := r.db.ExecContext(ctx, `
		DELETE c
		FROM channels AS c
		WHERE c.id = ?
	`, ID)
	if err != nil {
		return xerrors.Errorf("could not delete Channel %d: %w", ID, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not delete Channel %d: %w", ID, err)
	}

	return nil
}

func scanChannel(row *sql.Row) (*channel.Channel, error) {
	var c dbChannel

	err := row.Scan(
		&c.ID,
		&c.Link,
		&c.Country,
		&c.SourceID,
		&c.SourceHandler,
		&c.Name,
		&c.ImageURL,
		&c.Type,
		&c.FollowersCount,
		&c.FriendsCount,
		&c.LastRefreshedAt,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.NotFound
		}
		return nil, xerrors.Errorf("failed when scanning Channel: %w", err)
	}

	return c.toDomainEntity(), nil
}

func scanChannels(rows *sql.Rows) ([]*channel.Channel, error) {
	var cs []*channel.Channel

	for rows.Next() {
		var c dbChannel

		err := rows.Scan(
			&c.ID,
			&c.Link,
			&c.Country,
			&c.SourceID,
			&c.SourceHandler,
			&c.Name,
			&c.ImageURL,
			&c.Type,
			&c.FollowersCount,
			&c.FriendsCount,
			&c.LastRefreshedAt,
		)
		if err != nil {
			return nil, xerrors.Errorf("failed when scanning Channel: %w", err)
		}

		cs = append(cs, c.toDomainEntity())
	}

	if err := rows.Err(); err != nil {
		return nil, xerrors.Errorf("failed when scanning Channels: %w", err)
	}

	return cs, nil
}
