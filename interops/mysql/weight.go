package mysql

import (
	"context"
	"database/sql"

	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/errors"
	"github.com/xescugc/smf/interops/weight"
	"golang.org/x/xerrors"
)

// WeightRepository is the implementation of the weight.Repository
type WeightRepository struct {
	db *sql.DB
}

// dbWeight is the sql representation of the weight.Weight
type dbWeight struct {
	ID         sql.NullInt64
	Type       sql.NullString
	Percentage sql.NullInt64
}

// toDomainEntity transforms the dbWeight to a weight.Weight
func (w dbWeight) toDomainEntity() *weight.Weight {
	t, _ := channel.TypeString(w.Type.String)
	return &weight.Weight{
		ID:         int(w.ID.Int64),
		Type:       t,
		Percentage: int(w.Percentage.Int64),
	}
}

// NewWeightRepository returns a new WeightRepository
func NewWeightRepository(db *sql.DB) *WeightRepository {
	return &WeightRepository{
		db: db,
	}
}

// Create creates a new Weight w
func (r *WeightRepository) Create(ctx context.Context, w weight.Weight) (int, error) {
	res, err := r.db.ExecContext(ctx, `
		INSERT INTO weights (type, percentage)
		VALUES (?, ?)
	`, w.Type.String(), w.Percentage)
	if err != nil {
		return 0, xerrors.Errorf("error while inserting Weight %+v: %w", w, err)
	}

	id, err := lastInsertedID(res)
	if err != nil {
		return 0, xerrors.Errorf("error when getting Weight ID %+v: %w", w, err)
	}

	return id, nil
}

// Find returns the Weight with ID
func (r *WeightRepository) Find(ctx context.Context, ID int) (*weight.Weight, error) {
	row := r.db.QueryRowContext(ctx, `
		SELECT w.id, w.type, w.percentage
		FROM weights AS w
		WHERE w.id = ?
	`, ID)

	p, err := scanWeight(row)
	if err != nil {
		return nil, xerrors.Errorf("failed when scanning Weight %d: %w", ID, err)
	}

	return p, nil
}

// Filter returns all the Weights
func (r *WeightRepository) Filter(ctx context.Context) ([]*weight.Weight, error) {
	rows, err := r.db.QueryContext(ctx, `
		SELECT w.id, w.type, w.percentage
		FROM weights AS w
	`)
	if err != nil {
		return nil, xerrors.Errorf("could not filter Weights: %w", err)
	}
	defer rows.Close()

	ts, err := scanWeights(rows)
	if err != nil {
		return nil, xerrors.Errorf("could not scan Weights: %w", err)
	}

	return ts, nil
}

// Update updates the Weight with ID with Weight w
func (r *WeightRepository) Update(ctx context.Context, ID int, w weight.Weight) error {
	res, err := r.db.ExecContext(ctx, `
		UPDATE weights AS w
		SET w.type=?, w.percentage=?
		WHERE w.id = ?
	`, w.Type.String(), w.Percentage, ID)
	if err != nil {
		return xerrors.Errorf("could not update Weight %d %+v: %w", ID, w, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not update Weight %d %+v: %w", ID, w, err)
	}

	return nil
}

// Delete deltes the Weight with ID
func (r *WeightRepository) Delete(ctx context.Context, ID int) error {
	res, err := r.db.ExecContext(ctx, `
		DELETE w
		FROM weights AS w
		WHERE w.id = ?
	`, ID)
	if err != nil {
		return xerrors.Errorf("could not delete Weight %d: %w", ID, err)
	}

	if err = rowsAffected(res); err != nil {
		return xerrors.Errorf("could not delete Weight %d: %w", ID, err)
	}

	return nil
}

func scanWeight(row *sql.Row) (*weight.Weight, error) {
	var w dbWeight

	err := row.Scan(
		&w.ID,
		&w.Type,
		&w.Percentage,
	)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.NotFound
		}
		return nil, xerrors.Errorf("failed when scanning Weight: %w", err)
	}

	return w.toDomainEntity(), nil
}

func scanWeights(rows *sql.Rows) ([]*weight.Weight, error) {
	var ws []*weight.Weight

	for rows.Next() {
		var w dbWeight

		err := rows.Scan(
			&w.ID,
			&w.Type,
			&w.Percentage,
		)
		if err != nil {
			return nil, xerrors.Errorf("failed when scanning Weight: %w", err)
		}

		ws = append(ws, w.toDomainEntity())
	}

	if err := rows.Err(); err != nil {
		return nil, xerrors.Errorf("failed when scanning Weight: %w", err)
	}

	return ws, nil
}
