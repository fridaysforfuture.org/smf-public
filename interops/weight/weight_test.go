package weight_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xescugc/smf/interops/channel"
	"github.com/xescugc/smf/interops/weight"
)

func TestValidate(t *testing.T) {
	tests := []struct {
		Name   string
		Weight weight.Weight
		Err    string
	}{
		{
			Name: "Success",
			Weight: weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			},
		},
		{
			Name: "RequiredType",
			Weight: weight.Weight{
				Percentage: 10,
			},
			Err: "Type is required",
		},
		{
			Name: "InvalidType",
			Weight: weight.Weight{
				Percentage: 10,
				Type:       channel.Type(90),
			},
			Err: `Type "Type(90)" is invalid`,
		},
		{
			Name: "RequiredPercentage",
			Weight: weight.Weight{
				Type:       channel.Twitter,
				Percentage: 0,
			},
			Err: "Percentage is required",
		},
		{
			Name: "InvalidPercentage",
			Weight: weight.Weight{
				Type:       channel.Twitter,
				Percentage: 200,
			},
			Err: "Percentage can not be higher than 100",
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			err := tt.Weight.Validate()
			if err != nil {
				assert.Equal(t, tt.Err, err.Error())
			} else if tt.Err != "" {
				assert.Equal(t, tt.Err, "")
			}
		})
	}
}
