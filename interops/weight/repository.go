package weight

import "context"

//go:generate mockgen -destination=../mock/weight_repository.go -mock_names=Repository=WeightRepository -package mock github.com/xescugc/smf/interops/weight Repository

// Repository is the interface of all the action that can be done to a Weight
type Repository interface {
	Create(ctx context.Context, c Weight) (int, error)
	Find(ctx context.Context, ID int) (*Weight, error)
	Filter(ctx context.Context) ([]*Weight, error)
	Update(ctx context.Context, ID int, c Weight) error
	Delete(ctx context.Context, ID int) error
}
