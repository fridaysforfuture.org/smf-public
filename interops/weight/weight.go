package weight

import (
	"github.com/xescugc/smf/interops/channel"
	"golang.org/x/xerrors"
)

// Weight is the structure that defines the Weight data
type Weight struct {
	ID         int
	Type       channel.Type
	Percentage int
}

// Validate validates the Weight data
func (w *Weight) Validate() error {
	if w.Type == channel.NoType {
		return xerrors.New("Type is required")
	} else if !w.Type.IsAType() {
		return xerrors.Errorf("Type %q is invalid", w.Type)
	} else if w.Percentage == 0 {
		return xerrors.New("Percentage is required")
	} else if w.Percentage > 100 {
		return xerrors.New("Percentage can not be higher than 100")
	}

	return nil
}
