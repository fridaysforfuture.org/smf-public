package errors

import "golang.org/x/xerrors"

// List of all the general errors
var (
	NotFound = xerrors.New("not found")
)
