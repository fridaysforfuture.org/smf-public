package tier

import "golang.org/x/xerrors"

// Tier is the structure that defines the Tier data
type Tier struct {
	ID   int
	Name string

	MinChannelFollowersCount int
	MaxChannelFollowersCount int

	PostLikesCount    int
	PostSharesCount   int
	PostCommentsCount int
}

// Validate validates the Tier data
func (t *Tier) Validate() error {
	if t.Name == "" {
		return xerrors.New("Name is required")
	} else if t.MaxChannelFollowersCount == 0 && t.MinChannelFollowersCount == 0 {
		return xerrors.New("MaxChannelFollowersCount or MinChannelFollowersCount is required")
	} else if t.MaxChannelFollowersCount < t.MinChannelFollowersCount {
		return xerrors.New("MaxChannelFollowersCount has to be greater than MinChannelFollowersCount")
	} else if t.PostLikesCount == 0 {
		return xerrors.New("PostLikesCount is required")
	} else if t.PostSharesCount == 0 && t.PostCommentsCount == 0 {
		return xerrors.New("PostSharesCount or PostCommentsCount is required")
	}

	return nil
}
