package tier

import "context"

//go:generate mockgen -destination=../mock/tier_repository.go -mock_names=Repository=TierRepository -package mock github.com/xescugc/smf/interops/tier Repository

// Repository is the interface of all the action that can be done to a Tier
type Repository interface {
	Create(ctx context.Context, c Tier) (int, error)
	Find(ctx context.Context, ID int) (*Tier, error)
	Filter(ctx context.Context) ([]*Tier, error)
	Update(ctx context.Context, ID int, c Tier) error
	Delete(ctx context.Context, ID int) error
}
