package tier_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/xescugc/smf/interops/tier"
)

func TestValidate(t *testing.T) {
	tests := []struct {
		Name string
		Tier tier.Tier
		Err  string
	}{
		{
			Name: "Success",
			Tier: tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			},
		},
		{
			Name: "NoName",
			Tier: tier.Tier{
				Name:                     "",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			},
			Err: "Name is required",
		},
		{
			Name: "NoMinMaxChannelFollowersCount",
			Tier: tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 0,
				MaxChannelFollowersCount: 0,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			},
			Err: "MaxChannelFollowersCount or MinChannelFollowersCount is required",
		},
		{
			Name: "MinChannelFollowersCountGreater",
			Tier: tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 20,
				MaxChannelFollowersCount: 10,
				PostLikesCount:           10,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			},
			Err: "MaxChannelFollowersCount has to be greater than MinChannelFollowersCount",
		},
		{
			Name: "NoPostLikesCount",
			Tier: tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           0,
				PostSharesCount:          10,
				PostCommentsCount:        10,
			},
			Err: "PostLikesCount is required",
		},
		{
			Name: "NoPostSharesCountAndPostCommentsCount",
			Tier: tier.Tier{
				Name:                     "Name",
				MinChannelFollowersCount: 10,
				MaxChannelFollowersCount: 20,
				PostLikesCount:           10,
				PostSharesCount:          0,
				PostCommentsCount:        0,
			},
			Err: "PostSharesCount or PostCommentsCount is required",
		},
	}

	for _, tt := range tests {
		t.Run(tt.Name, func(t *testing.T) {
			err := tt.Tier.Validate()
			if err != nil {
				assert.Equal(t, tt.Err, err.Error())
			} else if tt.Err != "" {
				assert.Equal(t, tt.Err, "")
			}
		})
	}
}
