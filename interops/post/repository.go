package post

import (
	"context"
	"time"

	"github.com/xescugc/smf/interops/weight"
)

//go:generate mockgen -destination=../mock/post_repository.go -mock_names=Repository=PostRepository -package mock github.com/xescugc/smf/interops/post Repository

// Repository is the interface of all the action that can be done to a Post
type Repository interface {
	Create(ctx context.Context, cID int, c Post) (int, error)
	Find(ctx context.Context, ID int) (*Post, error)
	FindBySourceID(ctx context.Context, cID int, srcID string) (*Post, error)
	Filter(ctx context.Context) ([]*Post, error)
	FilterBySystems(ctx context.Context, ws []*weight.Weight) ([]*WithChannel, error)
	Update(ctx context.Context, ID int, c Post) error
	Delete(ctx context.Context, ID int) error
	DeleteByChannelID(ctx context.Context, cID int) error
	DeleteOlderThan(ctx context.Context, t time.Time) error
}
