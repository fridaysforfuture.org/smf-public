package post

import (
	"time"

	"github.com/xescugc/smf/interops/channel"
)

// Post is the structure that defines the Post data
type Post struct {
	ID int

	SourceID    string
	Title       string
	Link        string
	Description string
	Enabled     bool

	ImagesURLs  []string
	PublishedAt time.Time

	// We also have this on TW QuoteCount
	LikesCount    int
	SharesCount   int
	CommentsCount int
}

// WithChannel is  an aggregation between Post and Channel
type WithChannel struct {
	Post

	Channel channel.Channel
}
