.PHONY: db-cli
db-cli: ## Locally connects to the DB
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml exec mariadb mysql -uroot -proot123

.PHONY: test
test: mariadb-up ## Test everything
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml build scripts && \
		docker-compose -f docker/docker-compose.yml -f docker/develop.yml run --rm scripts go test ./...

.PHONY: mariadb-up
mariadb-up: ## Stats the MariaDB service
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml up -d mariadb

.PHONY: generate
generate: ## Regenerates files that need to be generated
	@go generate ./...

.PHONY: serve-admin
serve-admin: ## Serves the Admin SVC
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml up --build admin

.PHONY: serve-public
serve-public: ## Serves the Admin SVC
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml up --build public

.PHONY: serve
serve: ## Serves the Admin SVC
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml up --build public admin

.PHONY: migrate
migrate: mariadb-up ## Starts the MariaDB service
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml build scripts && \
		docker-compose -f docker/docker-compose.yml -f docker/develop.yml run --rm scripts go run scripts/migrate.go

.PHONY: logs
logs: ## Shows the logs
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml logs -f

.PHONY: down
down: ## Stops everything
	@docker-compose -f docker/docker-compose.yml -f docker/develop.yml down --remove-orphans
