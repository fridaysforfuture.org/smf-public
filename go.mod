module github.com/xescugc/smf

go 1.14

require (
	github.com/VividCortex/mysqlerr v0.0.0-20170204212430-6c6b55f8796f
	github.com/arschles/assert v2.0.0+incompatible // indirect
	github.com/arschles/go-bindata-html-template v0.0.0-20170123182818-839a6918b9ff
	github.com/contribsys/faktory v1.4.0-1
	github.com/contribsys/faktory_worker_go v1.0.1
	github.com/dghubble/go-twitter v0.0.0-20190719072343-39e5462e111f
	github.com/elazarl/go-bindata-assetfs v1.0.0
	github.com/go-kit/kit v0.10.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/mock v1.4.3
	github.com/gorilla/feeds v1.1.1
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.3
	github.com/lib/pq v1.3.0 // indirect
	github.com/lopezator/migrator v0.2.0
	github.com/spf13/pflag v1.0.5
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
	golang.org/x/xerrors v0.0.0-20191011141410-1b5146add898
	google.golang.org/api v0.3.1
)
