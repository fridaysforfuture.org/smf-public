package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"strings"

	"github.com/go-kit/kit/log"
	"github.com/gorilla/handlers"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"github.com/xescugc/smf/interops/mysql"
	"github.com/xescugc/smf/interops/mysql/migrate"
	"github.com/xescugc/smf/public/config"
	"github.com/xescugc/smf/public/service"
	transportHTTP "github.com/xescugc/smf/public/service/transport/http"
)

func init() {
	flag.String("db-host", "", "Database Host")
	flag.Int("db-port", 0, "Database Port")
	flag.String("db-user", "", "Database User")
	flag.String("db-password", "", "Database Password")
	flag.String("db-name", "", "Database Name")
	flag.Int("port", 4000, "Service Port")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
}

func main() {
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	// Initialize and validate the config
	var cfg config.Config
	if err := viper.Unmarshal(&cfg); err != nil {
		panic(err)
	}

	if err := cfg.Validate(); err != nil {
		panic(err)
	}

	// Initialize the Logger
	var logger log.Logger
	logger = log.NewLogfmtLogger(os.Stderr)
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "service", "public", "caller", log.DefaultCaller)

	// Initializing DB
	logger.Log("msg", "MariaDB starting ...")
	db, err := mysql.New(viper.GetString("db-host"), viper.GetInt("db-port"), viper.GetString("db-user"), viper.GetString("db-password"), mysql.Options{
		DBName:          viper.GetString("db-name"),
		MultiStatements: true,
		ClientFoundRows: true,
	})
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "MariaDB started")

	logger.Log("msg", "migrations running ...")
	err = migrate.Migrate(db)
	if err != nil {
		panic(err)
	}
	logger.Log("msg", "migrations finished")

	// Initializint Repositories
	pr := mysql.NewPostRepository(db)
	wr := mysql.NewWeightRepository(db)

	s := service.New(pr, wr)

	mux := http.NewServeMux()

	mux.Handle("/", transportHTTP.MakeHandler(s))

	http.Handle("/", handlers.LoggingHandler(os.Stdout, mux))

	logger.Log("msg", "HTTP", "addr", cfg.Port)
	logger.Log("err", http.ListenAndServe(fmt.Sprintf(":%d", cfg.Port), nil))
}
