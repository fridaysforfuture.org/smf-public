package service_test

import (
	"context"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"github.com/xescugc/smf/interops/channel"
	imock "github.com/xescugc/smf/interops/mock"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/interops/weight"
	"github.com/xescugc/smf/public/service"
)

func TestNew(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		ctrl := gomock.NewController(t)
		defer ctrl.Finish()

		pr := imock.NewPostRepository(ctrl)
		wr := imock.NewWeightRepository(ctrl)

		p := service.New(pr, wr)
		assert.NotNil(t, p)
		assert.Implements(t, (*service.Service)(nil), p)
	})
}

func TestGetPosts(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		var (
			s   = NewService(t)
			ctx = context.Background()
			p   = post.WithChannel{
				Post: post.Post{
					Title: "hi",
				},
			}
			w = weight.Weight{
				Type:       channel.Twitter,
				Percentage: 10,
			}
		)
		defer s.Finish()

		s.Weights.EXPECT().Filter(ctx).Return([]*weight.Weight{&w}, nil)
		s.Posts.EXPECT().FilterBySystems(ctx, []*weight.Weight{&w}).Return([]*post.WithChannel{&p}, nil)

		ps, err := s.S.GetPosts(ctx)
		require.NoError(t, err)
		assert.Equal(t, []*post.WithChannel{&p}, ps)
	})
}
