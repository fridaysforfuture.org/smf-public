package service_test

import (
	"testing"

	"github.com/golang/mock/gomock"
	imock "github.com/xescugc/smf/interops/mock"
	"github.com/xescugc/smf/public/service"
)

type Service struct {
	Posts   *imock.PostRepository
	Weights *imock.WeightRepository

	S service.Service

	Ctrl *gomock.Controller
}

func NewService(t *testing.T) Service {
	ctrl := gomock.NewController(t)

	pr := imock.NewPostRepository(ctrl)
	wr := imock.NewWeightRepository(ctrl)

	a := service.New(pr, wr)

	return Service{
		Posts:   pr,
		Weights: wr,

		S: a,

		Ctrl: ctrl,
	}
}

func (s *Service) Finish() {
	s.Ctrl.Finish()
}
