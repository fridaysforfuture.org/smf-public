package service

import (
	"context"

	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/interops/weight"
	"golang.org/x/xerrors"
)

//go:generate mockgen -destination=../mock/service.go -mock_names=Service=Service -package mock github.com/xescugc/smf/public/service Service

// Service is the main interface of the Public service
// with all the functions provided
type Service interface {
	GetPosts(ctx context.Context) ([]*post.WithChannel, error)
}

// Public is a struct that implements the Service
type Public struct {
	posts   post.Repository
	weights weight.Repository
}

// New returns a new Public which implements the Service interface
func New(pr post.Repository, wr weight.Repository) *Public {
	return &Public{
		posts:   pr,
		weights: wr,
	}
}

// GetPosts retuns a list of all the Posts
func (p *Public) GetPosts(ctx context.Context) ([]*post.WithChannel, error) {
	ws, err := p.weights.Filter(ctx)
	if err != nil {
		return nil, xerrors.Errorf("failed to filter Weights: %w", err)
	}

	ps, err := p.posts.FilterBySystems(ctx, ws)
	if err != nil {
		return nil, xerrors.Errorf("failed to filter Posts: %w", err)
	}

	return ps, nil
}
