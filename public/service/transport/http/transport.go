package http

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	kithttp "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/feeds"
	"github.com/gorilla/mux"
	"github.com/xescugc/smf/public/service"
	"github.com/xescugc/smf/public/service/transport/http/models"
)

// MakeHandler retuns an http.Handler of the Service s functions
func MakeHandler(s service.Service) http.Handler {
	postsIndexRSSHandler := kithttp.NewServer(
		makePostsIndexEndpoint(s),
		decodePostsIndexRequest,
		encodePostsIndexRSSResponse,
	)

	postsIndexJSONHandler := kithttp.NewServer(
		makePostsIndexEndpoint(s),
		decodePostsIndexRequest,
		encodePostsIndexJSONResponse,
	)

	r := mux.NewRouter()

	// We'll have different endpoints for RSS and JSON, we
	// could work with Headers and Contet-Type or by having
	// different URLs
	r.Handle("/rss", postsIndexRSSHandler).Methods("GET")
	r.Handle("/json", postsIndexJSONHandler).Methods("GET")

	return r
}

func decodePostsIndexRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return postsIndexRequest{}, nil
}

func encodePostsIndexRSSResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(postsIndexResponse)

	mainFeed := &feeds.Feed{
		Title:       "FridaysForFuture",
		Link:        &feeds.Link{Href: "https://www.fridaysforfuture.org/"},
		Description: "#FridaysForFuture is a movement that began in August 2018, after 15 years old Greta Thunberg sat in front of the Swedish parliament every schoolday for three weeks, to protest against the lack of action on the climate crisis. She posted what she was doing on Instagram and Twitter and it soon went viral.",
		//Author:      &feeds.Author{Name: "Jason Moiron", Email: "jmoiron@jmoiron.net"},
		Created: time.Now(),
		Items:   make([]*feeds.Item, 0, len(res.Posts)),
	}

	for _, p := range res.Posts {
		var img string
		if len(p.ImagesURLs) > 0 {
			img = p.ImagesURLs[0]
		}
		mainFeed.Items = append(mainFeed.Items, &feeds.Item{
			Title:       p.Title,
			Link:        &feeds.Link{Href: p.Link},
			Description: p.Description,

			Author:  &feeds.Author{Name: p.Channel.Name},
			Id:      fmt.Sprintf("%d", p.ID),
			Created: p.PublishedAt,
			Enclosure: &feeds.Enclosure{
				Url: img,
			},
		})
	}

	w.Header().Set("Content-Type", "application/rss+xml")
	mainFeed.WriteRss(w)

	return nil
}

func encodePostsIndexJSONResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	res := response.(postsIndexResponse)
	posts := make([]models.Post, 0, len(res.Posts))

	for _, p := range res.Posts {
		posts = append(posts, models.Post{
			ID:            p.ID,
			Title:         p.Title,
			Link:          p.Link,
			Description:   p.Description,
			ImagesURLs:    p.ImagesURLs,
			PublishedAt:   p.PublishedAt,
			LikesCount:    p.LikesCount,
			SharesCount:   p.SharesCount,
			CommentsCount: p.CommentsCount,
			Channel: models.Channel{
				ID:             p.Channel.ID,
				Link:           p.Channel.Link,
				Type:           p.Channel.Type.String(),
				Name:           p.Channel.Name,
				ImageURL:       p.Channel.ImageURL,
				FollowersCount: p.Channel.FollowersCount,
				FriendsCount:   p.Channel.FriendsCount,
			},
		})
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(posts)

	return nil
}
