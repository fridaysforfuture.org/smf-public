package models

import (
	"time"
)

// Post is the JSON representation of the Post
type Post struct {
	ID            int       `json:"id"`
	Title         string    `json:"title"`
	Link          string    `json:"link"`
	Description   string    `json:"description"`
	ImagesURLs    []string  `json:"images_urls"`
	PublishedAt   time.Time `json:"published_at"`
	LikesCount    int       `json:"likes_count"`
	SharesCount   int       `json:"shares_count"`
	CommentsCount int       `json:"comments_count"`

	Channel Channel `json:"channel"`
}

// Channel is the JSON representation of a Channel
type Channel struct {
	ID             int    `json:"id"`
	Link           string `json:"link"`
	Type           string `json:"type"`
	Name           string `json:"name"`
	ImageURL       string `json:"image_url"`
	FollowersCount int    `json:"followers_count"`
	FriendsCount   int    `jsno:"friends_count"`
}
