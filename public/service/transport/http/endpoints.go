package http

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/xescugc/smf/interops/post"
	"github.com/xescugc/smf/public/service"
)

type postsIndexRequest struct {
}

type postsIndexResponse struct {
	Posts []*post.WithChannel
}

func makePostsIndexEndpoint(s service.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(postsIndexRequest)
		ps, err := s.GetPosts(ctx)
		if err != nil {
			return nil, err
		}

		return postsIndexResponse{
			Posts: ps,
		}, nil
	}
}
