#!/bin/bash

# The use is 'bash deploy.sh worker admin' if you want to deploy 'worker' and
# 'admin' services, if you just want to update the 'data/' then just run
# the deploy empty and only the file will be uploaded

set -e

# Uses docker-macine to deploy and the machine is named 'smf'
eval $(docker-machine env smf)

# On the server this is the default folder so this is where it'll land
# and this is the default one that the docker will try to read the data from
docker-machine scp -r docker/data/ smf:/home/francesc

# It'll deploy all the services passed as parameter, if none
# is passed  no deploy of services will be done
if (( $# != 0 )); then
  docker-compose -f docker/docker-compose.yml -f docker/production.yml build $@
  docker-compose -f docker/docker-compose.yml -f docker/production.yml up --no-deps -d $@
fi

# Remove old images so we do not keep them if they are not needed
docker image prune -a -f --filter "until=24h"
