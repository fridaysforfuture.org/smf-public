package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"github.com/xescugc/smf/interops/mysql"
	"github.com/xescugc/smf/interops/mysql/migrate"
)

func init() {
	flag.String("db-host", "", "Database Host")
	flag.Int("db-port", 0, "Database Port")
	flag.String("db-user", "", "Database User")
	flag.String("db-password", "", "Database Password")
	flag.String("db-name", "", "Database Name")

	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}

	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)

	viper.AutomaticEnv()
	viper.SetEnvKeyReplacer(strings.NewReplacer("-", "_"))
}

func main() {
	pflag.Parse()
	viper.BindPFlags(pflag.CommandLine)

	db, err := mysql.New(viper.GetString("db-host"), viper.GetInt("db-port"), viper.GetString("db-user"), viper.GetString("db-password"), mysql.Options{})
	if err != nil {
		panic(err)
	}

	_, err = db.Exec(fmt.Sprintf("DROP DATABASE IF EXISTS %s", viper.GetString("db-name")))
	if err != nil {
		panic(err)
	}

	db.Close()

	db, err = mysql.New(viper.GetString("db-host"), viper.GetInt("db-port"), viper.GetString("db-user"), viper.GetString("db-password"), mysql.Options{
		DBName:          viper.GetString("db-name"),
		MultiStatements: true,
		ClientFoundRows: true,
	})
	if err != nil {
		panic(err)
	}

	if err := migrate.Migrate(db); err != nil {
		panic(err)
	}

	db.Close()
}
